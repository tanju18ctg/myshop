<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\user\StripeController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\ShippingAreaController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\frontend\CartController;
use App\Http\Controllers\user\CartPageController;
use App\Http\Controllers\user\CheckoutController;
use App\Http\Controllers\user\WishListController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ProductReviewController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\StockController;
use App\Http\Controllers\Admin\SubadminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Frontend\indexController;
use App\Http\Controllers\Frontend\LanguageController;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\Frontend\TrackingController;
use App\Http\Controllers\SslCommerzPaymentController;
use App\Http\Controllers\User\ReviewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

//=============================Admin-Routes===============================//
Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'auth', 'permission']], function () {

  Route::get('admin-dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
  Route::get('admin/profile', [AdminController::class, 'Profile'])->name('profile');
  Route::post('admin/update-data', [AdminController::class, 'updateData'])->name('update-data');
  Route::get('admin/image', [AdminController::class, 'adminImage'])->name('admin-image');
  Route::post('update-image', [AdminController::class, 'updateImage'])->name('update-image');
  Route::get('change-password', [AdminController::class, 'changePassword'])->name('change-password');
  Route::post('password-store', [AdminController::class, 'adminPasswordStore'])->name('adminPassword');

  // Read all users


  Route::get('all/users', [AdminController::class, 'readUsers'])->name('all-users');
  Route::get('user-banned/{user_id}', [AdminController::class, 'banned']);
  Route::get('user-unbanned/{user_id}', [AdminController::class, 'unbanned']);

  //-------------------brand-routes---------------------------------------//
  Route::get('brands', [BrandController::class, 'index'])->name('brands');
  Route::post('brand/store', [BrandController::class, 'store'])->name('brand-store');
  Route::get('admin/brand-edit/{id}', [BrandController::class, 'edit'])->name('brand-edit');
  Route::post('brand/update', [BrandController::class, 'update'])->name('brand-update');
  Route::get('brand-delete/{id}', [BrandController::class, 'delete'])->name('brand-delete');

  //----------------------categories-routes------------------------//

  Route::get('category', [CategoryController::class, 'index'])->name('category');
  Route::post('category-store', [CategoryController::class, 'store'])->name('store-category');
  Route::get('/category-edit/{id}', [CategoryController::class, 'edit'])->name('category-edit');
  Route::post('category/update', [CategoryController::class, 'update'])->name('category-update');
  Route::get('/category-delete/{id}', [CategoryController::class, 'delete'])->name('category-delete');


  //------------------sub-category----------------//
  Route::get('sub-category', [CategoryController::class, 'subCatindex'])->name('sub-category');
  Route::post('subcategory-store', [CategoryController::class, 'Substore'])->name('subcategory-store');
  Route::get('/subcategory-delete/{id}', [CategoryController::class, 'subCatdelete']);
  //-------------------sub-subcategories----------------//
  Route::get('sub-subcategory', [CategoryController::class, 'subSubCatindex'])->name('sub-subcategory');
  Route::get('subcategory/ajax/{cat_id}', [CategoryController::class, 'getSubCat']);
  Route::post('sub-subcategoryStore', [CategoryController::class, 'subSubCatStore'])->name('subsubcategory-store');
  Route::get('/subSubCategory-edit/{id}', [CategoryController::class, 'subSubEdit'])->name('subSubCategory-edit');
  Route::get('/subSubCategory-delete/{id}', [CategoryController::class, 'subSubDelete'])->name('subSubCategory-delete');
  Route::post('sub-subCategoryUpdate', [CategoryController::class, 'subSubCatUpdate'])->name('subSubCategory-update');

  //============================Products-routes========================//
  Route::get('add-product', [ProductController::class, 'addProduct'])->name('add-product');
  Route::get('subsubcategory/ajax/{subcat_id}', [ProductController::class, 'getSubSubCat']);
  Route::get('manage-product', [ProductController::class, 'index'])->name('manage-product');
  Route::post('product/store', [ProductController::class, 'productStore'])->name('product-store');
  Route::get('/product-edit/{id}', [ProductController::class, 'edit'])->name('product-edit');
  Route::post('/product-update', [ProductController::class, 'update'])->name('product-update');
  Route::post('/update-product-image', [ProductController::class, 'updateProductImage'])->name('update-product-image');
  Route::post('/update-product-thumbnail', [ProductController::class, 'updateProductThumb'])->name('update-product-thumbnail');
  Route::get('/multiImage-delete/{id}', [ProductController::class, 'deleteMultiImage']);
  Route::get('/product-inactive/{id}', [ProductController::class, 'inactive']);
  Route::get('/product-active/{id}', [ProductController::class, 'active']);

  //================================Slider-Routes==========================//
  Route::get('sliders', [SliderController::class, 'index'])->name('sliders');
  Route::post('slider/store', [SliderController::class, 'store'])->name('slider-store');
  Route::get('slider-edit/{id}', [SliderController::class, 'edit']);
  Route::post('slider/update', [SliderController::class, 'update'])->name('slider-update');
  Route::get('slider-delete/{id}', [SliderController::class, 'destroy']);
  Route::get('slider-inactive/{id}', [SliderController::class, 'inactive']);
  Route::get('slider-active/{id}', [SliderController::class, 'active']);

  //=============coupon======================//

  Route::get('coupon', [CouponController::class, 'create'])->name('coupon');
  Route::post('coupon-store', [CouponController::class, 'couponStore'])->name('coupon-store');
  Route::get('coupon-edit/{id}', [CouponController::class, 'edit'])->name('coupon-edit');
  Route::get('coupon-delete/{id}', [CouponController::class, 'destroy'])->name('coupon-delete');
  Route::post('coupon/update', [CouponController::class, 'update'])->name('coupon-update');

  //==================Shipping Area==================//

  //division routes
  Route::get('add/division', [ShippingAreaController::class, 'create'])->name('add-division');
  Route::post('store/division', [ShippingAreaController::class, 'divisionStore'])->name('store-division');
  Route::get('division-edit/{id}', [ShippingAreaController::class, 'divisionEdit']);
  Route::get('division-delete/{id}', [ShippingAreaController::class, 'divisionDestroy']);
  Route::post('division/update', [ShippingAreaController::class, 'divisionUpdate'])->name('update-division');

  //district routes
  Route::get('create/district', [ShippingAreaController::class, 'districtCreate'])->name('add-district');
  Route::post('store/district', [ShippingAreaController::class, 'districtStore'])->name('store-district');
  Route::get('district-edit/{id}', [ShippingAreaController::class, 'districtEdit']);
  Route::get('district-delete/{id}', [ShippingAreaController::class, 'districtDestroy']);
  Route::post('district/update', [ShippingAreaController::class, 'districtUpdate'])->name('update-district');
  Route::get('/district/ajax/{id}', [ShippingAreaController::class, 'getDistrictAjax']);

  //state routes
  Route::get('create/state', [ShippingAreaController::class, 'stateCreate'])->name('add-state');
  Route::post('store/state', [ShippingAreaController::class, 'stateStore'])->name('store-state');
  Route::get('state-edit/{id}', [ShippingAreaController::class, 'stateEdit']);
  Route::get('state-delete/{id}', [ShippingAreaController::class, 'stateDestroy']);
  Route::post('state/update', [ShippingAreaController::class, 'stateUpdate'])->name('update-state');

  //orders
  Route::get('pending-orders', [OrderController::class, 'pendingIndex'])->name('pending-orders');
  Route::get('order-view/{order_id}', [OrderController::class, 'orderView'])->name('order-view');
  Route::get('confirmed-orders', [OrderController::class, 'confirmedIndex'])->name('confirmed-orders');
  Route::get('processing-orders', [OrderController::class, 'processingIndex'])->name('processing-orders');
  Route::get('picked-orders', [OrderController::class, 'pickedIndex'])->name('picked-orders');
  Route::get('shipped-orders', [OrderController::class, 'shippedIndex'])->name('shipped-orders');
  Route::get('delevered-orders', [OrderController::class, 'deleveredIndex'])->name('delevered-orders');

  Route::get('pending-to-cancel/{order_id}', [OrderController::class, 'cancelOrder'])->name('pending-to-cancel');
  Route::get('cancel-orders', [OrderController::class, 'cancelIndex'])->name('cancel-orders');

  Route::get('generate-pdf/{order_id}', [OrderController::class, 'downloadInvoice']);
  Route::get('pending-to-confirm/{order_id}', [OrderController::class, 'pendingToConfirm'])->name('pending-to-confirm');
  Route::get('confirm-to-processing/{order_id}', [OrderController::class, 'confirmToProcessing'])->name('confirm-to-processing');
  Route::get('processing-to-picked/{order_id}', [OrderController::class, 'processingToPicked'])->name('processing-to-picked');
  Route::get('picked-to-shipped/{order_id}', [OrderController::class, 'pickedToShipped'])->name('picked-to-shipped');
  Route::get('shipped-to-delevered/{order_id}', [OrderController::class, 'shippedToDelevered'])->name('shipped-to-delevered');
  Route::get('reports', [ReportController::class, 'index'])->name('reports');
  Route::post('search-date', [ReportController::class, 'searchDate'])->name('search-date');
  Route::post('search-month', [ReportController::class, 'searchMonth'])->name('search-month');
  Route::post('search-year', [ReportController::class, 'searchYear'])->name('search-year');
  Route::get('product-review-approve', [ProductReviewController::class, 'create'])->name('product-review-approve');
  Route::get('review/delete/{product_id}', [ProductReviewController::class, 'destroy'])->name('review-delete');
  Route::get('review-approve/{product_id}', [ProductReviewController::class, 'approve']);

  Route::get('product-stock', [StockController::class, 'index'])->name('product-stock');
  Route::get('stock-edit/{id}', [StockController::class, 'edit']);
  Route::post('stock/update/{id}', [StockController::class, 'update'])->name('stock-update');


  //------Role Management--------//
  Route::resource('role', RoleController::class);

  //--------permission-management--------------//
  Route::resource('permission', PermissionController::class);


  // subadmin-management //

  Route::resource('subadmin', SubadminController::class);

});


//Language Routes

Route::get('language/english', [LanguageController::class, 'english'])->name('language-english');
Route::get('language/bangla', [LanguageController::class, 'bangla'])->name('language-bangla');
Route::get('single-product-details/{id}/{slug}', [IndexController::class, 'singleProduct']);

//-product-tags Route
Route::get('product-tag/{tag}', [IndexController::class, 'tagWiseProduct']);

//-subcategoryWiseProduct
Route::get('subcategory/product/{subcat_id}/{slug}', [IndexController::class, 'subcatWiseProduct']);
Route::get('subsubcategory/product/{subsubcat_id}/{slug}', [IndexController::class, 'subsubcatWiseProduct']);

//product view modal with ajax
Route::get('product/view/modal/{id}', [IndexController::class, 'productView']);

//product add to cart
Route::post('add/to/cart/{id}', [CartController::class, 'addToCart']);

//Product mini cart
Route::get('product/mini/cart', [CartController::class, 'miniCart']);
Route::get('minicart/remove/{rowId}', [CartController::class, 'miniCartRemove']);





//=============================User-Routes===============================//
Route::group(['prefix' => 'user', 'middleware' => ['user', 'auth']], function () {
  Route::get('dashboard', [UserController::class, 'index'])->name('user.dashboard');
  Route::get('update/profile', [UserController::class, 'updateData'])->name('update-profile');
  Route::get('user/image', [UserController::class, 'userImage'])->name('user-image');
  Route::post('user-update/image', [UserController::class, 'imageUpdate'])->name('user-update-image');
  Route::get('update/password/change', [UserController::class, 'updatePassChange'])->name('update-password');
  Route::post('update/password', [UserController::class, 'passwordStore'])->name('password-store');
  Route::get('user/order', [UserController::class, 'orderCreate'])->name('user-order');
  Route::get('order-view/{order_id}', [UserController::class, 'orderView']);
  Route::get('invoice-download/{order_id}', [UserController::class, 'downloadInvoice']);
  Route::get('generate-pdf/{order_id}', [UserController::class, 'generatePDF']);
  Route::post('user/return/request-submit', [UserController::class, 'returnSubmit'])->name('user-return-request');
  Route::get('user-return-order', [UserController::class, 'returnOrderView'])->name('user-return-order');
  Route::get('cancel/order', [UserController::class, 'cancelOrder'])->name('cancel-order');



  Route::post('review-store', [ReviewController::class, 'reviewStore'])->name('review-store');
  Route::get('product-review/{product_id}', [ReviewController::class, 'reviewCreate'])->name('product-review');
});
Route::post('stripe/payment', [StripeController::class, 'store'])->name('stripe-order');


Route::group([ 'middleware' => ['user', 'auth']], function () {
  // SSLCOMMERZ Start
  Route::get('/example1', [SslCommerzPaymentController::class, 'exampleEasyCheckout']);
  Route::get('/example2', [SslCommerzPaymentController::class, 'exampleHostedCheckout']);

  Route::post('/pay', [SslCommerzPaymentController::class, 'index']);
  Route::post('/pay-via-ajax', [SslCommerzPaymentController::class, 'payViaAjax']);

  Route::post('/success', [SslCommerzPaymentController::class, 'success']);
  Route::post('/fail', [SslCommerzPaymentController::class, 'fail']);
  Route::post('/cancel', [SslCommerzPaymentController::class, 'cancel']);

  Route::post('/ipn', [SslCommerzPaymentController::class, 'ipn']);
  //SSLCOMMERZ END
});



// Wish List Routes
Route::post('add/wishlist/{id}', [CartController::class, 'addToWish']);
Route::get('get-wishlist', [WishListController::class, 'getWishList']);
Route::get('wishList/remove/{id}', [WishListController::class, 'wishListRemove']);
Route::get('wishList', [WishListController::class, 'create'])->name('wishList');


//My cart Routes
Route::get('my-cart', [CartController::class, 'create'])->name('cart');
Route::get('/get-cartProduct', [CartController::class, 'getcart']);
Route::get('/cart/remove/{id}', [CartController::class, 'destroy']);

Route::get('/cart/increment/{rowId}', [CartController::class, 'increment']);
Route::get('/cart/decrement/{rowId}', [CartController::class, 'decrement']);
Route::get('/', [indexController::class, 'index']);


Route::post('/coupon/apply', [CartController::class, 'couponApply']);
Route::get('/coupon/calculate', [CartController::class, 'couponCalculate']);
Route::get('/coupon/remove', [CartController::class, 'couponRemove']);


// process checkout
Route::post('checkout/store', [CheckoutController::class, 'checkoutStore'])->name('checkout-store');
Route::get('process/checkout', [CartController::class, 'createCheckout'])->name('process-checkout');
Route::get('/state/ajax/{id}', [CheckoutController::class, 'stateWithAjax']);
Route::get('division/ajax/{id}', [CheckoutController::class, 'districtWithAjax']);

// SOCIALITE

Route::get('login/google', [LoginController::class, 'googleRedirect'])->name('login-google');
Route::get('login/google/callback', [LoginController::class, 'googleCallback']);

Route::get('login/facebook', [LoginController::class, 'facebookRedirect'])->name('login-facebook');
Route::get('login/facebook/callback', [LoginController::class, 'facebookCallback']);

Route::post('order/tracking', [TrackingController::class, 'orderTracking'])->name('order-tracking');


//serch product


Route::get('search/product', [SearchController::class, 'productSearch'])->name('search-result');
Route::get('/home', [SearchController::class, 'index']);




Route::get('find-product', [SearchController::class, 'findProduct']);
