<style> 

.design{
    padding-top: 10px;
}

.design-li{
    list-style: none;
    padding: 0 20px;
    border-bottom: .5px solid rgb(212, 211, 211);
}


.design-li:hover{
    background : #f3f3f3;
    cursor: pointer;
}

.design-li a:hover{
    color: #333333;
}

</style>


<ul class="design"> 
    @forelse ($products as $product)

        <a href="{{ url('single-product-details/' . $product->id . '/' . $product->product_slug_en) }}">
            <li class="design-li">
                <img src="{{ asset($product->product_thumbnail) }}" alt="product" width="60px;" height="auto">
                <strong>{{ $product->product_name_en }}</strong>
            </li>    
            
        </a>    
        @empty
        <li style="color:red; padding:0 20px">Not Found !</li>
    @endforelse
</ul>