@foreach ($product_tags as $product)
<div class="row product-list-row">
    <div class="col col-sm-4 col-lg-4">
        <div class="product-image">
            <div class="image">
                <img src="{{ asset($product->product_thumbnail) }}"
                    alt="">
            </div>
        </div><!-- /.product-image -->
    </div><!-- /.col -->
    <div class="col col-sm-8 col-lg-8">
        <div class="product-info">
            <h3 class="name"><a href="detail.html">
                    @if (session()->get('language') == 'english')
                        {{ $product->product_name_en }}
                    @else
                        {{ $product->product_name_bn }}
                    @endif
                </a></h3>
            <div class="rating rateit-small"></div>
            <div class="product-price">
                @if ($product->discount_price == null)
                    <span
                        class="price">৳{{ $product->selling_price }}
                    </span>
                @else
                    <span
                        class="price">৳{{ $product->discount_price }}
                    </span>
                    <span
                        class="price-before-discount">{{ $product->selling_price }}
                    </span>
                @endif
            </div><!-- /.product-price -->
            <div class="description m-t-10">Suspendisse posuere arcu
                diam,
                id accumsan eros pharetra ac. Nulla enim risus,
                facilisis
                bibendum gravida eget, lacinia id purus. Suspendisse
                posuere
                arcu diam, id accumsan eros pharetra ac. Nulla enim
                risus,
                facilisis bibendum gravida eget.
            </div>
            <div class="cart clearfix animate-effect">
                <div class="action">
                    <ul class="list-unstyled">
                        <li class="add-cart-button btn-group">
                            <button class="btn btn-primary icon"
                                type="button" data-toggle="modal"
                                data-target="#cartModal"
                                id="{{ $product->id }}"
                                onclick="viewProduct(this.id)">
                                <i class="fa fa-shopping-cart"></i>
                            </button>
                            <button class="btn btn-primary cart-btn"
                                type="button">Add to
                                cart</button>

                        </li>

                        <button class="btn btn-success icon"
                            type="button" id="{{ $product->id }}"
                            onclick="addToWishList(this.id)">
                            <i class="icon fa fa-heart"></i>
                        </button>

                        <li class="lnk">
                            <a class="add-to-cart" href="detail.html"
                                title="Compare">
                                <i class="fa fa-signal"
                                    aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.action -->
            </div><!-- /.cart -->

        </div><!-- /.product-info -->
    </div><!-- /.col -->
</div><!-- /.product-list-row -->
@endforeach