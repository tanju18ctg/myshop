@extends('layouts.frontend-master')

@section('title')
    Check out
@endsection
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class='active'>Check out</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="checkout-box ">
                <div class="row">
                    <form action="{{ route('checkout-store') }}" method="post">
                        @csrf
                        <div class="col-md-8">
                            <div class="panel-group checkout-steps" id="accordion">
                                <!-- checkout-step-01  -->
                                <div class="panel panel-default checkout-step-01">

                                    <div id="collapseOne" class="panel-collapse collapse in">

                                        <!-- panel-body  -->
                                        <div class="panel-body">
                                            <div class="row">

                                                <!-- already-registered-login -->
                                                <div class="col-md-6 col-sm-6 already-registered-login">
                                                    <h3 class="checkout-subtitle">Shipping Address</h3>

                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputEmail1"> Name
                                                            <span>*</span></label>
                                                        <input type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="exampleInputEmail1" placeholder="name" name="shipping_name"
                                                            value="{{ Auth::user()->name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputPassword1">Email
                                                            <span>*</span></label>
                                                        <input type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="exampleInputPassword1" placeholder="shipping_email"
                                                            name="shipping_email" value="{{ Auth::user()->email }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputPassword1">Phone
                                                            <span>*</span></label>
                                                        <input type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="exampleInputPassword1" placeholder="Phone"
                                                            name="shipping_phone" value="{{ Auth::user()->phone }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputPassword1">Post Code
                                                            <span>*</span></label>
                                                        <input type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="exampleInputPassword1" placeholder="post code"
                                                            name="post_code">
                                                    </div>


                                                </div>
                                                <!-- already-registered-login -->

                                                <div class="col-md-6 col-sm-6 already-registered-login">

                                                    <div class="form-group">
                                                        <label class="form-control-label">Select Division<span
                                                                class="tx-danger">*</span></label>
                                                        <select class="form-control select2"
                                                            data-placeholder="Choose Browser" id="division_id"
                                                            name="division_id">
                                                            <option value="">Select Division</option>
                                                            @foreach ($divisions as $division)
                                                                <option value="{{ $division->id }}">
                                                                    {{ $division->division_name }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('division_id')
                                                            <span class="text-danger"> {{ $message }} </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="form-control-label">Select District<span
                                                                class="tx-danger">*</span></label>
                                                        <select class="form-control select2"
                                                            data-placeholder="Choose Browser" id="district_id"
                                                            name="district_id">

                                                        </select>
                                                        @error('district_id')
                                                            <span class="text-danger"> {{ $message }} </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Select State<span
                                                                class="tx-danger">*</span></label>
                                                        <select class="form-control select2"
                                                            data-placeholder="Choose Browser" id="state_id"
                                                            name="state_id">

                                                        </select>
                                                        @error('state_id')
                                                            <span class="text-danger"> {{ $message }} </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-control-label">Notes<span
                                                                class="tx-danger">*</span></label>
                                                        <textarea name="notes" id="" cols="30" rows="5" class="form-control"></textarea>

                                                        @error('state_id')
                                                            <span class="text-danger"> {{ $message }} </span>
                                                        @enderror
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                        <!-- panel-body  -->

                                    </div><!-- row -->
                                </div>
                                <!-- checkout-step-01  -->
                            </div><!-- /.checkout-steps -->
                        </div>
                        <div class="col-md-4">
                            <!-- checkout-progress-sidebar -->
                            <div class="checkout-progress-sidebar ">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">Your Checkout Progress</h4>
                                        </div>
                                        <div class="">
                                            <ul class="nav nav-checkout-progress list-unstyled">
                                                @foreach ($carts as $item)
                                                    <li>
                                                        <strong> Image: </strong>
                                                        <img src="{{ asset($item->options->image) }}" alt="product"
                                                            style="width:50px; height:50px">
                                                        <strong>Qty: </strong> {{ $item->qty }}
                                                        <strong>Size: </strong> {{ $item->options->size }}
                                                        <strong> Color: </strong> {{ $item->options->color }}
                                                        <hr>
                                                    </li>
                                                @endforeach
                                                <li>
                                                    @if (Session::has('coupon'))
                                                        <strong>Coupon: </strong>
                                                        {{ session::get('coupon')['coupon_name'] }}({{ Session::get('coupon')['coupon_discount'] }})%
                                                        <br>
                                                        <strong> Discount Amount: </strong>
                                                        {{ session::get('coupon')['discount_amount'] }} <br>
                                                        <strong>Sub Total: </strong>{{ $total }} <br>
                                                        <strong>Grand Total: </strong>{{ $total }} <br>
                                                    @else
                                                        <strong>Sub Total: </strong>{{ $total }} <br>
                                                        <strong>Grand Total: </strong>{{ $total }} <br>
                                                    @endif
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- checkout-progress-sidebar -->
                        </div>
                        <div class="col-md-4">
                            <!-- checkout-progress-sidebar -->
                            <div class="checkout-progress-sidebar ">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">Select Payment Method</h4>
                                        </div>
                                        <div class="">
                                            <ul class="nav nav-checkout-progress list-unstyled">

                                                <li>
                                                    <input type="radio" name="payment_method" id="stripe"
                                                        value="stripe">
                                                    <label for="stripe">Stripe</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="payment_method" id="card"
                                                        value="sslHost">
                                                    <label for="card">Ssl Host Payment</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="payment_method" id="hand"
                                                        value="sslEasy">
                                                    <label for="hand">Ssl Easy Payment</label>
                                                </li>

                                                <button type="submit"
                                                    class="btn-upper btn btn-primary checkout-page-button pull-right">Payment
                                                    Process</button>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- checkout-progress-sidebar -->
                        </div>
                    </form>
                </div><!-- /.row -->
            </div><!-- /.checkout-box -->
            <!-- =================== BRANDS CAROUSEL =================================== -->

            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div><!-- /.container -->
    </div><!-- /.body-content -->


    <script src="{{ asset('backend/lib/jquery/jquery.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#division_id').on('change', function() {
                var division_id = $(this).val();

                if (division_id) {
                    $.ajax({
                        url: "{{ url('/division/ajax/') }}/" + division_id,
                        type: 'GET',
                        dataType: 'json',
                        success: function(data) {
                            $('#district_id').empty();
                            $('#state_id').html('');
                            $.each(data, function(key, value) {
                                $('#district_id').append('<option value="' + value.id +
                                    '">' + value.district_name + '</option>');
                            })
                        },

                    });
                } else {
                    alert('danger');
                }
            })

        })

        // get State
        $(document).ready(function() {
            $('#district_id').on('change', function() {
                var district_id = $(this).val();
                if (district_id) {
                    $.ajax({
                        url: "{{ url('/state/ajax') }}/" + district_id,
                        type: 'GET',
                        dataType: 'json',
                        success: function(data) {
                            $('#state_id').html('');
                            $.each(data, function(key, value) {
                                $('#state_id').append('<option value="' + value.id +
                                    '">' + value.state_name + '</option>');
                            })
                        },

                    });
                } else {
                    alert('danger');
                }
            })

        })
    </script>
@endsection
