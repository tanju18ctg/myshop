<div class="modal fade" id="cartModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> <span id="pname"></span> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModal">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row"> 
                <div class="col-md-4">
                    <div class="cart-img-top"> 
                        <img src="" alt="product" id="pimage" style="width:200px; height:auto">
                    </div>
                </div>

                <div class="col-md-4">
                    <ul class="list-group">
                        <li class="list-group-item">Price: <strong class="text-danger"> 
                            $ <span id="pprice"></span> 
                            $ <del id="oldprice"></del>
                        </strong> </li>
                        <li class="list-group-item"> Product Code: <strong id="pcode"></strong>  </li>
                        <li class="list-group-item">Category: <strong id="pcategory"></strong> </li>
                        <li class="list-group-item">Brand: <strong id="pbrand"></strong>  </li>
                        <li class="list-group-item">Stock: 
                            <span class="badge badge-pill badge-success" id="pstock" style="background-color: green; color:#fff"> </span> 
                            <span class="badge badge-pill badge-danger" id="stockout" style="background-color: red; color:#fff"> </span> 
                        </li>
                      </ul>
                </div>

                <div class="col-md-4">

                      <div class="form-group"> 
                        <label for="color">Select Color</label>
                            <select class="form-control" aria-label="Default select example" id="color" name="color">

                          </select>
                      </div>
                      <div class="form-group" id="size_area"> 
                        <label for="size">Select Size</label>
                            <select class="form-control" aria-label="Default select example" id="size" name="size"></select>
                      </div>

                      <div class="form-group"> 
                            <label for="qty">Quantity</label>
                            <input type="number" name="quantity" class="form-control" id="qty" value="1" min="1">
                      </div>
                      <input type="hidden" name="id" id="product_id">
                      <button type="button" class="btn btn-primary" onclick="addToCart()">Add to Cart</button>
          
                </div>
            </div>
        </div>
        <div class="modal-footer">
            
        </div>
    </div>
    </div>
</div>

