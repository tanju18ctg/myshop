@foreach ($product_tags as $product)
<div class="col-sm-6 col-md-4 wow fadeInUp">
    <div class="products">
        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="detail.html"><img
                            src="{{ asset($product->product_thumbnail) }}"
                            alt=""></a>
                </div><!-- /.image -->

                <div class="tag new">
                    <span>
                        @php
                            $discountAmount = $product->selling_price - $product->discount_price;
                            $discountParcent = ($product->discount_price / $discountAmount) * 100;
                        @endphp
                        @if ($product->discount_price == null)
                            new
                        @else
                            @if (session()->get('language') == 'english')
                                {{ round($discountParcent) }}%
                            @else
                                {{ round($discountParcent) }}%
                            @endif
                        @endif
                    </span>
                </div>
            </div><!-- /.product-image -->
            <div class="product-info text-left">
                <h3 class="name"><a href="detail.html">
                        @if (session()->get('language') == 'english')
                            {{ $product->product_name_en }}
                        @else
                            {{ $product->product_name_bn }}
                        @endif
                    </a>
                </h3>
                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
                    @if ($product->discount_price == null)
                        <span class="price">৳{{ $product->selling_price }}
                        </span>
                    @else
                        <span class="price">৳{{ $product->discount_price }}
                        </span>
                        <span
                            class="price-before-discount">{{ $product->selling_price }}
                        </span>
                    @endif
                </div><!-- /.product-price -->

            </div><!-- /.product-info -->
            <div class="cart clearfix animate-effect">
                <div class="action">
                    <ul class="list-unstyled">
                        <li class="add-cart-button btn-group">
                            <button class="btn btn-primary icon"
                                type="button" data-toggle="modal"
                                data-target="#cartModal"
                                id="{{ $product->id }}"
                                onclick="viewProduct(this.id)">
                                <i class="fa fa-shopping-cart"></i>
                            </button>
                            <button class="btn btn-primary cart-btn"
                                type="button">Add to
                                cart</button>

                        </li>

                        <button class="btn btn-success icon" type="button"
                            id="{{ $product->id }}"
                            onclick="addToWishList(this.id)">
                            <i class="icon fa fa-heart"></i>
                        </button>

                        <li class="lnk">
                            <a class="add-to-cart" href="detail.html"
                                title="Compare">
                                <i class="fa fa-signal"
                                    aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.action -->
            </div><!-- /.cart -->
        </div><!-- /.product -->
    </div><!-- /.products -->
</div><!-- /.item -->
@endforeach