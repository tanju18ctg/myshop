@extends('layouts.frontend-master')


@section('content')
    <!---english to bangla -->
    @php
    function str_bangla($str_bn)
    {
        $en = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $bn = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];

        return $str_bn = str_replace($en, $bn, $str_bn);
    }
    @endphp

    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='active'>Handbags</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                <div class='col-md-3 sidebar'>
                    <!-- ================================== TOP NAVIGATION ================================== -->

                    @include('frontend.inc.categories');

                    <!-- ================================== TOP NAVIGATION : END ================================== -->
                    <div class="sidebar-module-container">

                        <div class="sidebar-filter">
                            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                            <div class="sidebar-widget wow fadeInUp">
                                <h3 class="section-title">shop by</h3>
                                <div class="widget-header">
                                    <h4 class="widget-title">Category</h4>
                                </div>
                                <div class="sidebar-widget-body">
                                    <div class="accordion">

                                        @foreach ($categories as $cat)
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a href="#collapse{{ $cat->id }}" data-toggle="collapse"
                                                        class="accordion-toggle collapsed">
                                                        @if (session()->get('language') == 'bangla')
                                                            {{ $cat->category_name_bn }}
                                                        @else
                                                            {{ $cat->category_name_en }}
                                                        @endif
                                                    </a>
                                                </div><!-- /.accordion-heading -->
                                                <div class="accordion-body collapse" id="collapse{{ $cat->id }}"
                                                    style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        @php
                                                            $subcategories = App\Models\Subcategory::where('category_id', $cat->id)
                                                                ->orderBy('subcategory_name_en', 'ASC')
                                                                ->get();
                                                        @endphp
                                                        <ul>
                                                            @foreach ($subcategories as $subcat)
                                                                @if (session()->get('language') == 'bangla')
                                                                    <li><a
                                                                            href="#">{{ $subcat->subcategory_name_bn }}</a>
                                                                    </li>
                                                                @else
                                                                    <li><a
                                                                            href="#">{{ $subcat->subcategory_name_en }}</a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div><!-- /.accordion-inner -->
                                                </div><!-- /.accordion-body -->
                                            </div><!-- /.accordion-group -->
                                        @endforeach


                                    </div><!-- /.accordion -->
                                </div><!-- /.sidebar-widget-body -->
                            </div><!-- /.sidebar-widget -->
                            <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->

                            <!-- ============================================== PRICE SILDER============================================== -->
                           
                            <!-- ============================================== PRICE SILDER : END ============================================== -->

                            <!-- ============================================== PRODUCT TAGS ============================================== -->
                            @include('frontend.inc.product-tag')
                            <!-- ============================================== PRODUCT TAGS : END ============================================== -->
                            <!--- ============================================== Testimonials============================================== -->
                            <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                                <div id="advertisement" class="advertisement">
                                    <div class="item">
                                        <div class="avatar"><img src="assets/images/testimonials/member1.png"
                                                alt="Image"></div>
                                        <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port
                                            mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                        <div class="clients_author">John Doe <span>Abc Company</span> </div>
                                        <!-- /.container-fluid -->
                                    </div><!-- /.item -->

                                    <div class="item">
                                        <div class="avatar"><img src="assets/images/testimonials/member3.png"
                                                alt="Image"></div>
                                        <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port
                                            mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                        <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>
                                    </div><!-- /.item -->

                                    <div class="item">
                                        <div class="avatar"><img src="assets/images/testimonials/member2.png"
                                                alt="Image"></div>
                                        <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port
                                            mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                        <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span> </div>
                                        <!-- /.container-fluid -->
                                    </div><!-- /.item -->

                                </div><!-- /.owl-carousel -->
                            </div>

                            <!-- ============================================== Testimonials: END ============================================== -->

                            <div class="home-banner">
                                <img src="assets/images/banners/LHS-banner.jpg" alt="Image">
                            </div>

                        </div><!-- /.sidebar-filter -->
                    </div><!-- /.sidebar-module-container -->
                </div><!-- /.sidebar -->
                <div class='col-md-9'>
                    <!-- ========================================== SECTION – HERO ========================================= -->

                    <div id="category" class="category-carousel hidden-xs">
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('frontend') }}/assets/images/banners/cat-banner-1.jpg" alt=""
                                    class="img-responsive">
                            </div>
                            <div class="container-fluid">
                                <div class="caption vertical-top text-left">
                                    {{-- <div class="big-text">
						Big Sale
					</div> --}}

                                    {{-- <div class="excerpt hidden-sm hidden-md">
						Save up to 49% off
					</div> --}}
                                    {{-- <div class="excerpt-normal hidden-sm hidden-md">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit
					</div> --}}

                                </div><!-- /.caption -->
                            </div><!-- /.container-fluid -->
                        </div>
                    </div>




                    <!-- ========================================= SECTION – HERO : END ========================================= -->
                    <div class="clearfix filters-container m-t-10">
                        <div class="row">
                            <div class="col col-sm-6 col-md-2">
                                <div class="filter-tabs">
                                    <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                        <li class="active">
                                            <a data-toggle="tab" href="#grid-container"><i
                                                    class="icon fa fa-th-large"></i>Grid</a>
                                        </li>
                                        <li><a data-toggle="tab" href="#list-container"><i
                                                    class="icon fa fa-th-list"></i>List</a></li>
                                    </ul>
                                </div><!-- /.filter-tabs -->
                            </div><!-- /.col -->
                            <div class="col col-sm-12 col-md-6">
                                <div class="col col-sm-3 col-md-6 no-padding">
                                    <div class="lbl-cnt">

                                        <select name="" id="sortBy" class="form-control">
                                            <option value="sortBy">SortBy</option>
                                            <option value="priceLowToHigh" {{ ($sort == 'priceLowToHigh') ? 'selected' : '' }}>Price:Low to High</option>
                                            <option value="priceHighToLow" {{ ($sort == 'priceHighToLow') ? 'selected' : '' }}>Price:High To Low</option>
                                            <option value="nameAtoZ" {{ ($sort == 'nameAtoZ') ? 'selected' : '' }}>Product Name: A to Z</option>
                                            <option value="nameZtoA" {{ ($sort == 'nameZtoA') ? 'selected' : '' }}>Product Name: Z to A</option>
                                        </select>
                                        
                                       
                                    </div><!-- /.lbl-cnt -->
                                </div><!-- /.col -->
                               
                            </div><!-- /.col -->
                            
                        </div><!-- /.row -->
                    </div>

                    <div class="search-result-container ">
                        <div id="myTabContent" class="tab-content category-list">
                            <div class="tab-pane active " id="grid-container">
                                <div class="category-product">
                                    <div class="row" id="grid-view-product">
                                        @include('frontend.inc.grid-view-product')
                                    </div><!-- /.row -->
                                </div><!-- /.category-product -->
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane " id="list-container">
                                <div class="category-product">
                                    <div class="category-product-inner wow fadeInUp">
                                        <div class="products">
                                            <div class="product-list product" id="list-view-product">
                                                @include('frontend.inc.list-view-product');
                                                <div class="tag new"><span>new</span></div>
                                            </div><!-- /.product-list -->
                                        </div><!-- /.products -->
                                    </div><!-- /.category-product-inner -->
                                </div><!-- /.category-product -->
                            </div><!-- /.tab-pane #list-container -->
                        </div><!-- /.tab-content -->
                    </div><!-- /.search-result-container -->
                </div><!-- /.col -->
                <div class="ajax-loadmore-product text-center" style="display: none">
                    <img src="{{ asset('frontend/assets/images/loadmore.gif') }}" alt="" style="width:10%; margin-left:auto; margin-right:auto; display:block">
                </div>
            </div><!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <div id="brands-carousel" class="logo-slider wow fadeInUp">


                <div class="logo-slider-inner">
                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                        <div class="item m-t-15">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item m-t-10">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif"
                                    alt="">
                            </a>
                        </div>
                        <!--/.item-->
                    </div><!-- /.owl-carousel #logo-slider -->
                </div><!-- /.logo-slider-inner -->

            </div><!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div><!-- /.container -->

    </div><!-- /.body-content -->

    @section('scripts')
        <script> 
            $(document).on('change', '#sortBy', function(e){
                let sortBy = $("#sortBy").val();
                window.location = "{{ url(''.$route.'') }}/{{ $subCatId }}/{{ $subCatSlug }}?sort="+sortBy;
            })
        </script>

    {{-- <script> 
        function loadMoreProducts(){
            $.ajax({
                    type: "get",
                    url: "?page="+page,
                    beforeSend: function(response) {
                        $('.ajax-loadmore-product').show();
                    }
                })


            .done(function(data){
                if(data.grid_view == " " || data.list_view == " "){
                    $(".ajax-loadmore-product").html("No more product Found");
                    return;
                }

                $(".ajax-loadmore-product").hide();
                $("#grid-view-product").append(data.grid_view);
                $("#list-view-product").append(data.list_view);

            });

            .fail(function(){
                alert("Something Went Wrong");
            })
        }

        var page = 1;

        $(window).scroll( function(){
            if($(window).scrollTop() + $(window).height() >= $(document).height())
            {
                page++;
                loadMoreProducts(page);
            }
        })
    </script> --}}

    <script>
        function loadmoreProduct(page) {
            $.ajax({
                    type: "get",
                    url: "?page="+page,
                    beforeSend: function(response) {
                        $('.ajax-loadmore-product').show();
                    }
                })

                .done(function(data) {
                    if (data.grid_view == " " || data.list_view == " ") {
                        $('.ajax-loadmore-product').html('No More Product Found');
                        return;
                    }
                    $('.ajax-loadmore-product').hide();

                    $('#grid-view-product').append(data.grid_view);
                    $('#list-view-product').append(data.list_view);
                })

                .fail(function() {
                    alert('something went wrong')
                });

        }
        var page = 1;
        $(window).scroll(function() {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                loadmoreProduct(page);
            }
        });

    </script>
    @endsection
@endsection
