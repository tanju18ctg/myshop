<div class="card" style="width: 18rem;">
            <img class="img-thumbnail" src="{{ asset(Auth::user()->image) }}" alt="Card image cap">
         
                <ul class="list-group list-group-flush">
                    <a href="{{ route('user.dashboard') }}" class="btn btn-primary btn-block">Home</a>
                    <a href="{{ route('user-image')}}" class="btn btn-primary btn-block">Update Image</a>
                    <a href="{{ route('user-order')}}" class="btn btn-primary btn-block">My Orders</a>
                    <a href="{{ route('user-return-order')}}" class="btn btn-primary btn-block">Return Orders</a>
                    <a href="{{ route('cancel-order')}}" class="btn btn-primary btn-block">Cancel Orders</a>
                    <a href="{{ route('update-password')}}" class="btn btn-primary btn-block">Update Password</a>
                    <a class="btn-primary btn-block text-center" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"> Log Out</a>
                </ul>
      
</div>