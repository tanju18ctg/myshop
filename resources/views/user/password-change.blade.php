@extends('layouts.frontend-master')

@section('content')

<div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="home.html">Home</a></li>
                    <li class='active'>Login</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
        <div class="sign-in-page">
            <div class="row">
                <div class="col-sm-6">
                    @include('user.inc.slide-bar');
                </div>
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Hi: {{ Auth::user()->name}} <span> Update Your Password</span> </h3>
                        
                        <form action="{{ route('password-store') }}" method="post"> 
                            @csrf
                            <div class="form-group"> 
                                <input type="password" name="old_password" class="form-control" placeholder="old password">
                                @error('old_password')
                                    {{$message}}
                                @enderror
                            </div>
                            <div class="form-group"> 
                                <input type="password" name="new_password" class="form-control" placeholder="new password">
                                @error('new_password')
                                    {{$message}}
                                @enderror
                            </div>
                            <div class="form-group"> 
                                <input type="password" name="confirm_password" class="form-control" placeholder="old confirm_password">
                                @error('confirm_password')
                                    {{$message}}
                                @enderror
                            </div>


                            <button class="btn btn-success btn-sm">Change Password </button>

                        </form>
                    </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
    </div>
@endsection
