@extends('layouts.frontend-master')

@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="home.html">Home</a></li>
                    <li class='active'>Login</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="sign-in-page">
                <div class="row">
                    <div class="col-md-3">
                        @include('user.inc.slide-bar');
                    </div>
                    <div class="col-md-9 mt-2">
                        <div id="review" class="tab-pane">
                            <div class="product-tab">

                                <div class="product-add-review">
                                    <h4 class="title">Write your own review</h4>
                                    <div class="review-table">
                                        <form action="{{ route('review-store') }}" class="cnt-form" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $id }}">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th class="cell-label">&nbsp;</th>
                                                            <th>1 star</th>
                                                            <th>2 stars</th>
                                                            <th>3 stars</th>
                                                            <th>4 stars</th>
                                                            <th>5 stars</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="cell-label">Rating</td>
                                                            <td><input type="radio" name="rating" class="radio"
                                                                    value="1"></td>
                                                            <td><input type="radio" name="rating" class="radio"
                                                                    value="2"></td>
                                                            <td><input type="radio" name="rating" class="radio"
                                                                    value="3"></td>
                                                            <td><input type="radio" name="rating" class="radio"
                                                                    value="4"></td>
                                                            <td><input type="radio" name="rating" class="radio"
                                                                    value="5"></td>
                                                        </tr>

                                                    </tbody>
                                                </table><!-- /.table .table-bordered -->
                                            </div><!-- /.table-responsive -->
                                    </div><!-- /.review-table -->

                                    <div class="review-form">
                                        <div class="form-container">


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputReview">Review <span
                                                                class="astk">*</span></label>
                                                        <textarea class="form-control txt txt-review" name="comment" id="exampleInputReview" rows="4" placeholder=""></textarea>
                                                    </div><!-- /.form-group -->
                                                </div>
                                            </div><!-- /.row -->

                                            <div class="action text-left">
                                                <button class="btn btn-primary btn-upper">SUBMIT
                                                    REVIEW</button>
                                            </div><!-- /.action -->

                                            </form><!-- /.cnt-form -->
                                        </div><!-- /.form-container -->
                                    </div><!-- /.review-form -->

                                </div><!-- /.product-add-review -->

                            </div><!-- /.product-tab -->
                        </div><!-- /.tab-pane -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
