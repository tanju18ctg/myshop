@extends('layouts.frontend-master')

@section('content')

<div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="home.html">Home</a></li>
                    <li class='active'>Login</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
        <div class="sign-in-page">
            <div class="row">

                <div class="col-sm-6">
                    @include('user.inc.slide-bar');
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Hi: {{ Auth::user()->name}} <span> Update Profile</span> </h3>
                            
                            <form action="{{ route('user-update-image') }}" method="post" enctype="multipart/form-data"> 

                                @csrf 
                                <input type="hidden" name="old_image" value="{{ Auth::user()->image }}">
                                <div class="form-group"> 
                                    <input type="file" name="image" class="form-control">
                                    @error('name')
                                        {{$message}}
                                    @enderror
                                </div>

                                <button class="btn btn-success btn-sm"> Update </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
        </div>
    </div>
@endsection
