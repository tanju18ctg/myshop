@extends('layouts.admin-master')

@section('shipping')
    active show-sub
@endsection

@section('addDivision')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Shipping state </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page------->

                <!---------Add-create-page--------> 
                <div class="col-md-6 m-auto"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Update State</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('update-state')}}" method="post"> 
                            @csrf

                            <div class="form-group">
                              <label class="form-control-label">Select Division<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" name="division_id" id="division_id">
                                  <option value="">Select Division</option>
                                  @foreach ($divisions as $division )
                                    <option value="{{ $division->id }}" {{ $division->id == $state->division_id ? 'selected' : '' }}>{{ $division->division_name }}</option>
                                  @endforeach
                              </select>
                              @error('division_id')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Select District<span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="state_name" value="{{ $state->district->district_name }}" disabled>
                              </div>

                            <div class="form-group">
                                <label class="form-control-label">state Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="state_name" value="{{ $state->state_name }}" placeholder="Enter Division Name">
                                @error('state_name')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                                <input type="hidden" name="id" value="{{ $state->id }}">
                              <button class="btn btn-info mg-r-5" type="submit">Update state</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>

    <script src="{{ asset('backend/lib/jquery/jquery.js') }}"> </script>

<script type="text/javascript"> 
  $(document).ready(function () {
    $('#division_id').on('change', function(){
          var division_id = $(this).val();
           if(division_id)
           {
              $.ajax({
                url : "{{ url('/admin/district/ajax') }}/"+division_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                   var d = $('#district_id').empty();
                   $.each(data, function(key, value){
                      $('#district_id').append('<option value="'+value.id+'">'+ value.district_name + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })
  })
</script>

    <script>
        $(function(){
          'use strict';
  
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
        })
      </script>
@endsection