@extends('layouts.admin-master')

@section('users')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> All Users {{ count($users ) }} </span>
            @php
                $online_user = 0;
            @endphp
            @foreach ($users as $user)
                @php
                    if($user->userIsOnline()){
                        $online_user +=1; 
                    }
                @endphp
                
            @endforeach
           <span> Active Users  </span>  <span class="badge badge-pill badge-success"> {{ $online_user }} </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-12 m-auto">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr style="background: #181819;">
                                    <td class="col-md-3 text-center">
                                        <label for="">Image</label>
                                    </td>

                                    <td class="col-md-3 text-center">
                                        <label for="">Name</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Email</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Phone</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Staus</label>
                                    </td>
                                    <td class="col-md-2 text-center">
                                        <label for="">Account</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Action</label>
                                    </td>

                                </tr>

                                @foreach ($users as $user)
                                    <tr>
                                        <td class="col-md-3 text-center">
                                            <div class="product-name text-center">
                                                <img src="{{ asset($user->image) }}" alt=""
                                                    style="height: 100px; width:80px">
                                            </div>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $user->name }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $user->email }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $user->phone }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            @if ($user->userIsOnline())
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">{{ Carbon\Carbon::parse($user->last_seen)->diffForHumans()}}</span>
                                            @endif
                                        </td>

                                        <td class="col-md-2 text-center">
                                            @if ($user->isban == 0)
                                                <span class="badge badge-pill badge-primary">Unbanned</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Banned</span>
                                            @endif

                                        </td>

                                        <td class="col-md-2">
                                            @if ($user->isban == 0)
                                                <a href="{{ url('admin/user-banned/' . $user->id) }}"
                                                    class="btn btn-sm btn-danger"><i class="fa fa-arrow-down">Banned</i></a>
                                            @else
                                                <a href="{{ url('admin/user-unbanned/' . $user->id) }}"
                                                    class="btn btn-sm btn-primary"><i
                                                        class="fa fa-arrow-up">Unbanned</i></a>
                                            @endif

                                        </td>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
