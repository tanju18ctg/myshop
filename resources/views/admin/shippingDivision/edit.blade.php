@extends('layouts.admin-master')

@section('shipping')
    active show-sub
@endsection

@section('addDivision')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Shipping Division </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 

                <!---------Add-create-page--------> 
                <div class="col-md-6 m-auto"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Update Division</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('update-division')}}" method="post"> 
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label">Division Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="division_name" value="{{ $division->division_name }}" placeholder="Enter Division Name">
                                @error('division_name')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                                <input type="hidden" name="id" value="{{ $division->id }}">
                              <button class="btn btn-info mg-r-5" type="submit">Update</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection