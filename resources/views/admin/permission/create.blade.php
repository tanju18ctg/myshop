@extends('layouts.admin-master')

@section('permission')
    active show-sub
@endsection

@section('add-permission')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Brands </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!---------Add-create-page-------->
                <div class="col-md-12 m-auto">
                    <div class="card">
                        <div class="card-header">
                            <h5> Add New Role</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('permission.store') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                                <select name="role_id" id="role" class="form-control">
                                                    <option value="">Select Role</option>
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endforeach
                                                </select>
                                            @error('role')
                                                <span class="text-danger"> {{ $message }}</span>
                                            @enderror
                                        </div>
        
                                        <div class="form-layout-footer">
                                            <button class="btn btn-info mg-r-5" type="submit">Add Permission</button>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <table class="table table-bordered table-responsive table-danger"> 
                                            <thead> 
                                                <tr>
                                                    <th>Permission</th>
                                                    <th>Add</th>
                                                    <th>View</th>
                                                    <th>Edit</th>
                                                    <th>List</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Slider</td>
                                                    <td><input type="checkbox" name="permission[slider][add]"
                                                            @isset($permission['permission']['slider']['add']) checked @endisset
                                                            value="1"></td>
                                                    <td><input type="checkbox" name="permission[slider][view]"
                                                            @isset($permission['permission']['slider']['view']) checked @endisset
                                                            value="1"></td>
                                                    <td><input type="checkbox" name="permission[slider][edit]"
                                                            @isset($permission['permission']['slider']['edit']) checked @endisset
                                                            value="1"></td>
                                                    <td><input type="checkbox" name="permission[slider][list]"
                                                            @isset($permission['permission']['slider']['list']) checked @endisset
                                                            value="1"></td>
                                                    <td><input type="checkbox" name="permission[slider][delete]"
                                                            @isset($permission['permission']['slider']['delete']) checked @endisset
                                                            value="1"></td>
                                                </tr>
                                                <tr>
                                                    <td>Brand</td>
                                                    <td><input type="checkbox"
                                                            name="permission[brand][add]"
                                                            value="1">
                                                    </td>

                                                    <td><input type="checkbox"
                                                            name="permission[brand][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[brand][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[brand][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[brand][delete]"
                                                            value="1"></td>
                                                </tr>
                                                <tr>
                                                    <td>Categories</td>
                                                    <td><input type="checkbox"
                                                            name="permission[categories][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[categories][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[categories][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[categories][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[categories][delete]"
                                                            value="1"></td>
                                                </tr>
                                                <tr>
                                                    <td>Sub Categories</td>
                                                    <td><input type="checkbox"
                                                            name="permission[subCategories][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[subCategories][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[subCategories][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[subCategories][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[subCategories][delete]"
                                                            value="1"></td>
                                                </tr>
                                                <tr>
                                                    <td>Sub Sub-categories</td>
                                                    <td><input type="checkbox"
                                                            name="permission[subSubCategories][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[subSubCategories][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[subSubCategories][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[subSubCategories][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[subSubCategories][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Product</td>
                                                    <td><input type="checkbox"
                                                            name="permission[product][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[product][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[product][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[product][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[product][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Coupon</td>
                                                    <td><input type="checkbox"
                                                            name="permission[coupon][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[coupon][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[coupon][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[coupon][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[coupon][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td> Division</td>
                                                    <td><input type="checkbox"
                                                            name="permission[division][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[division][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[division][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[division][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[division][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>District</td>
                                                    <td><input type="checkbox"
                                                            name="permission[district][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[district][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[district][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[district][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[district][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>State</td>
                                                    <td><input type="checkbox"
                                                            name="permission[state][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[state][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[state][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[state][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[state][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Order</td>
                                                    <td><input type="checkbox"
                                                            name="permission[order][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[order][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[order][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[order][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[order][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Report</td>
                                                    <td><input type="checkbox"
                                                            name="permission[report][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[report][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[report][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[report][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[report][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Review</td>
                                                    <td><input type="checkbox"
                                                            name="permission[review][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[review][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[review][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[review][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[review][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Stock</td>
                                                    <td><input type="checkbox"
                                                            name="permission[stock][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[stock][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[stock][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[stock][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[stock][delete]"
                                                            value="1"></td>

                                                </tr>
                                                <tr>
                                                    <td>Role</td>
                                                    <td><input type="checkbox"
                                                            name="permission[role][add]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[role][view]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[role][edit]"
                                                            value="1"></td>

                                                    <td><input type="checkbox"
                                                            name="permission[role][list]"
                                                            value="1"></td>
                                                    <td><input type="checkbox"
                                                            name="permission[role][delete]"
                                                            value="1"></td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                              
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
