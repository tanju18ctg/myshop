@extends('layouts.admin-master')

@section('permission')
    active show-sub
@endsection

@section('add-permission')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Brands </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!---------Add-create-page-------->
                <div class="col-md-12 m-auto">
                    <div class="card">
                        <div class="card-header">
                            <h5> Edit Role</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('permission.update', $permission->id) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="role_id" id="role" class="form-control">
                                                <option value="">Select Role</option>
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}"
                                                        {{ $permission->role_id == $role->id ? 'selected' : '' }}>
                                                        {{ $role->name }} </option>
                                                @endforeach
                                            </select>
                                            @error('role')
                                                <span class="text-danger"> {{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-layout-footer">
                                            <button class="btn btn-info mg-r-5" type="submit">Update Permission</button>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <table class="table table-bordered">

                                                <thead>
                                                    <tr>
                                                        <th>Permission</th>
                                                        <th>Add</th>
                                                        <th>View</th>
                                                        <th>Edit</th>
                                                        <th>List</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Slider</td>
                                                        <td><input type="checkbox" name="permission[slider][add]"
                                                                @isset($permission['permission']['slider']['add']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox" name="permission[slider][view]"
                                                                @isset($permission['permission']['slider']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox" name="permission[slider][edit]"
                                                                @isset($permission['permission']['slider']['edit']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox" name="permission[slider][list]"
                                                                @isset($permission['permission']['slider']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox" name="permission[slider][delete]"
                                                                @isset($permission['permission']['slider']['delete']) checked @endisset
                                                                value="1"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brand</td>
                                                        <td><input type="checkbox"
                                                                name="permission[brand][add]"@isset($permission['permission']['brand']['add']) checked @endisset
                                                                value="1">
                                                        </td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[brand][view]"@isset($permission['permission']['brand']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[brand][edit]"@isset($permission['permission']['brand']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[brand][list]"@isset($permission['permission']['brand']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[brand][delete]"@isset($permission['permission']['brand']['delete']) checked @endisset
                                                                value="1"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Categories</td>
                                                        <td><input type="checkbox"
                                                                name="permission[categories][add]"@isset($permission['permission']['categories']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[categories][view]"@isset($permission['permission']['categories']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[categories][edit]"@isset($permission['permission']['categories']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[categories][list]"@isset($permission['permission']['categories']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[categories][delete]"@isset($permission['permission']['categories']['delete']) checked @endisset
                                                                value="1"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sub Categories</td>
                                                        <td><input type="checkbox"
                                                                name="permission[subCategories][add]"@isset($permission['permission']['subCategories']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[subCategories][view]"@isset($permission['permission']['subCategories']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[subCategories][edit]"@isset($permission['permission']['subCategories']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[subCategories][list]"@isset($permission['permission']['subCategories']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[subCategories][delete]"@isset($permission['permission']['subCategories']['delete']) checked @endisset
                                                                value="1"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sub Sub-categories</td>
                                                        <td><input type="checkbox"
                                                                name="permission[subSubCategories][add]"@isset($permission['permission']['subSubCategories']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[subSubCategories][view]"@isset($permission['permission']['subSubCategories']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[subSubCategories][edit]"@isset($permission['permission']['subSubCategories']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[subSubCategories][list]"@isset($permission['permission']['subSubCategories']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[subSubCategories][delete]"@isset($permission['permission']['subSubCategories']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Product</td>
                                                        <td><input type="checkbox"
                                                                name="permission[product][add]"@isset($permission['permission']['product']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[product][view]"@isset($permission['permission']['product']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[product][edit]"@isset($permission['permission']['product']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[product][list]"@isset($permission['permission']['product']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[product][delete]"@isset($permission['permission']['product']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Coupon</td>
                                                        <td><input type="checkbox"
                                                                name="permission[coupon][add]"@isset($permission['permission']['coupon']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[coupon][view]"@isset($permission['permission']['coupon']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[coupon][edit]"@isset($permission['permission']['coupon']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[coupon][list]"@isset($permission['permission']['coupon']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[coupon][delete]"@isset($permission['permission']['coupon']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td> Division</td>
                                                        <td><input type="checkbox"
                                                                name="permission[division][add]"@isset($permission['permission']['division']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[division][view]"@isset($permission['permission']['division']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[division][edit]"@isset($permission['permission']['division']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[division][list]"@isset($permission['permission']['division']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[division][delete]"@isset($permission['permission']['division']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>District</td>
                                                        <td><input type="checkbox"
                                                                name="permission[district][add]"@isset($permission['permission']['district']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[district][view]"@isset($permission['permission']['district']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[district][edit]"@isset($permission['permission']['district']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[district][list]"@isset($permission['permission']['subCategories']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[district][delete]"@isset($permission['permission']['district']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>State</td>
                                                        <td><input type="checkbox"
                                                                name="permission[state][add]"@isset($permission['permission']['state']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[state][view]"@isset($permission['permission']['state']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[state][edit]"@isset($permission['permission']['state']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[state][list]"@isset($permission['permission']['state']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[state][delete]"@isset($permission['permission']['state']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Order</td>
                                                        <td><input type="checkbox"
                                                                name="permission[order][add]"@isset($permission['permission']['order']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[order][view]"@isset($permission['permission']['order']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[order][edit]"@isset($permission['permission']['order']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[order][list]"@isset($permission['permission']['order']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[order][delete]"@isset($permission['permission']['order']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Report</td>
                                                        <td><input type="checkbox"
                                                                name="permission[report][add]"@isset($permission['permission']['report']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[report][view]"@isset($permission['permission']['report']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[report][edit]"@isset($permission['permission']['report']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[report][list]"@isset($permission['permission']['report']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[report][delete]"@isset($permission['permission']['report']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Review</td>
                                                        <td><input type="checkbox"
                                                                name="permission[review][add]"@isset($permission['permission']['review']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[review][view]"@isset($permission['permission']['review']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[review][edit]"@isset($permission['permission']['review']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[review][list]"@isset($permission['permission']['review']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[review][delete]"@isset($permission['permission']['review']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Stock</td>
                                                        <td><input type="checkbox"
                                                                name="permission[stock][add]"@isset($permission['permission']['stock']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[stock][view]"@isset($permission['permission']['stock']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[stock][edit]"@isset($permission['permission']['stock']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[stock][List]"@isset($permission['permission']['stock']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[stock][delete]"@isset($permission['permission']['stock']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                    <tr>
                                                        <td>Role</td>
                                                        <td><input type="checkbox"
                                                                name="permission[role][add]"@isset($permission['permission']['role']['add']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[role][view]"@isset($permission['permission']['role']['view']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[role][edit]"@isset($permission['permission']['role']['edit']) checked @endisset
                                                                value="1"></td>
    
                                                        <td><input type="checkbox"
                                                                name="permission[role][List]"@isset($permission['permission']['role']['list']) checked @endisset
                                                                value="1"></td>
                                                        <td><input type="checkbox"
                                                                name="permission[role][delete]"@isset($permission['permission']['role']['delete']) checked @endisset
                                                                value="1"></td>
    
                                                    </tr>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
