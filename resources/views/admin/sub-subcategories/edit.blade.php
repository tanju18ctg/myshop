@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">
   
        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Update sub Sub Category</h6>
                <form action="{{ route('subSubCategory-update') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Category Name En: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="subsubCategory_name_en" value="{{ $subSubCat->subsubCategory_name_en }}" placeholder="Enter firstname">
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Category Name Bn: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="subsubCategory_name_bn" value="{{ $subSubCat->subSubCategory_name_bn }}" placeholder="Enter lastname">
                        </div>
                      </div><!-- col-4 -->
               
                    
                    </div><!-- row -->
        
                    <div class="form-layout-footer">
                      <input type="hidden" name="id" value="{{ $subSubCat->id }}">
                      <button  type="submit" class="btn btn-success mg-r-5">Update</button>
  
                    </div><!-- form-layout-footer -->
                  </div><!-- form-layout -->
                </form>
              </div><!-- card -->

        </div>
    </div>
@endsection