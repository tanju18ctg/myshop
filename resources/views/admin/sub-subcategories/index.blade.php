@extends('layouts.admin-master')

@section('categories')
      active show-sub
@endsection

@section('subSubCategory')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Sub Sub Categoreis </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-8"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-responsive-md">
                          <table id="datatable1" class="table">
                            <thead>
                              <tr>
                                <th class="wd-25p"> Category Name</th>
                                <th class="wd-25p"> subCategory Name</th>
                                <th class="wd-25p"> Sub- Sub Category Name En </th>
                                <th class="wd-25p"> Sub- Sub Category Name Bn </th>
                                <th class="wd-25p"> Action </th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($sub_subCategories as $item )
                                <tr>
                                    <td> {{ $item->category->category_name_en}} </td>
                                    <td> {{$item->subCategory->subcategory_name_en }} </td>
                                    <td>{{ $item->subsubCategory_name_en }}</td>
                                    <td>{{ $item->subSubCategory_name_bn }}</td>
                                    <td>
                                      <a href="{{ url('admin/subSubCategory-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-pencil"> </i></a>
                                      <a href="{{ url('admin/subSubCategory-delete/'.$item->id) }}" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-trash"> </i> </a>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

                <!---------Add-create-page--------> 
                <div class="col-md-4"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Sub Sub Category</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('subsubcategory-store')}}" method="post"> 
                            @csrf
                            <div class="form-group">
                              <label class="form-control-label">Select Category<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" id="category_id" name="category_id">
                                  <option value="">Select Category</option>
                                  @foreach ($categories as $item )
                                    <option value="{{ $item->id }}">{{ $item->category_name_en }}</option>
                                  @endforeach
                              </select>
                              @error('category_name_en')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label class="form-control-label">Select Sub Category<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" id="subcategory_id" name="subcategory_id">
                                  <option value="">Select Category</option>
                              </select>
                              @error('subcategory_name_en')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">subSubcategory Name English: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="subsubCategory_name_en" value="{{ old('subcategory_name_en') }}" placeholder="Enter subCategory Name English">
                                @error('subsubCategory_name_en')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                              </div>

                          <div class="form-group">
                              <label class="form-control-label">subSubcategory Name Bangla: <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="subSubCategory_name_bn" value="{{ old('subcategory_name_bn') }}" placeholder="Enter subCategory Name Bangla">
                              @error('subSubCategory_name_bn')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Save </button>
                            </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>

<!-------------jquery-library--------------------> 

<script src="{{ asset('backend/lib/jquery/jquery.js') }}"> </script>

<script type="text/javascript"> 
  $(document).ready(function () {
    $('#category_id').on('change', function(){
          var cat_id = $(this).val();
           if(cat_id)
           {
              $.ajax({
                url : "{{ url('/admin/subcategory/ajax') }}/"+cat_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                   var d = $('#subcategory_id').empty();
                   $.each(data, function(key, value){
                      $('#subcategory_id').append('<option value="'+value.id+'">'+ value.subcategory_name_en + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })
  })
</script>

    <script>
        $(function(){
          'use strict';
  
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
        })
      </script>
@endsection