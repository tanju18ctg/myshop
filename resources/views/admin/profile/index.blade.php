@extends('layouts.admin-master')

@section('content')
<div class="container">

        <div class="body-content">
            <div class="container">
                <div class="sign-in-page">
                    <div class="row">
                        <div class="col-sm-6">
                            @include('admin.inc.slide-bar');
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title">Hi: <span> Update Profile</span> </h3>
                                    
                                    <form action="{{ route('update-data') }}" method="post"> 
                                        @csrf 
                                        <div class="form-group"> 
                                            <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}">
                                            @error('name')
                                                {{$message}}
                                            @enderror
                                        </div>
                                        <div class="form-group"> 
                                            <input type="text" name="phone" class="form-control" value="{{Auth::user()->phone}}">
                                            @error('phone')
                                                {{$message}}
                                            @enderror
                                        </div>

                                        <div class="form-group"> 
                                            <input type="text" name="email" class="form-control" value="{{ Auth::user()->email}}">
                                            @error('email')
                                                {{$message}}
                                            @enderror
                                        </div>
                                        <button class="btn btn-success btn-sm"> Update </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>


</div>
@endsection