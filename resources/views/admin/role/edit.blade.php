@extends('layouts.admin-master')

@section('role')
    active show-sub
@endsection

@section('add-role')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Brands </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!---------Add-create-page-------->
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">
                            <h5> Edit Role</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('role.update', $role->id) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label class="form-control-label">Role Name: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        value="{{ $role->name }}" placeholder="Enter Name">
                                    @error('name')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button class="btn btn-info mg-r-5" type="submit">Update Role</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
