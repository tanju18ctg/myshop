@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">

        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Update Stock Product</h6>
                <form action="{{ route('stock-update', $product->id) }}" method="post">
                    @csrf
                    <div class="form-layout">
                        <div class="row mg-b-25">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label">Product Name English: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="product_name_en"
                                        value="{{ $product->product_name_en }}" placeholder="Enter Product Name English">
                                    @error('product_name_en')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div><!-- col-4 -->

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label">Product Code : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="product_code"
                                        value="{{ $product->product_code }}" placeholder="Enter Product code">
                                    @error('product_code')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label">Product Quantity : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="product_qty"
                                        value="{{ $product->product_qty }}" placeholder="Enter Product Quantity">
                                    @error('product_qty')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>
                            </div><!-- col-4 -->
                        </div>

                        <div class="form-layout-footer text-center">
                            <button type="submit" class="btn btn-success mg-r-5">Stock Update</button>
                        </div><!-- form-layout-footer -->

                    </div>
            </div><!-- form-layout -->
        </form>

        </div><!-- card -->

    </div>
    </div>
@endsection
