<div class="card" style="width: 18rem;">
    <img class="img-thumbnail" src="{{ asset(Auth::user()->image) }}" alt="Card image cap">
         
                <ul class="list-group list-group-flush">
                    <a href="{{ route('admin.dashboard') }}" class="btn btn-primary btn-block">Home</a>
                    <a href="{{ route('admin-image')}}" class="btn btn-primary btn-block">Update Image</a>
                    <a href="{{ route('change-password')}}" class="btn btn-primary btn-block">Update Password</a>
                    <a class="btn-primary btn-block text-center" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"> Log Out</a>
                </ul>
      </div>