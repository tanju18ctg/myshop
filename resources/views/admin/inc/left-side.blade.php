<div class="sl-sideleft-menu">
    <a href="{{ route('admin.dashboard') }}" class="sl-menu-link">
        <div class="sl-menu-item">
            <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span class="menu-item-label">Dashboard</span>
        </div><!-- menu-item -->
    </a><!-- sl-menu-link -->
    @isset(Auth::user()->role->permission['permission']['slider']['list'])
        <a href="{{ route('sliders') }}" class="sl-menu-link @yield('sliders')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Sliders</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
    @endisset

    @isset(Auth::user()->role->permission['permission']['brand']['list'])
        <a href="{{ route('brands') }}" class="sl-menu-link @yield('brands')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Brands</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
    @endisset
    <a href="{{ url('/') }}" class="sl-menu-link" target="_blank">
        <div class="sl-menu-item">
            <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span class="menu-item-label">Visit Site</span>
        </div><!-- menu-item -->
    </a><!-- sl-menu-link -->


    @isset(Auth::user()->role->permission['permission']['categories']['list'])
        <a href="#" class="sl-menu-link @yield('categories')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Categories</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            
            <li class="nav-item"><a href="{{ route('category') }}" class="nav-link @yield('addCategory')">Add Category</a></li>
            @isset(Auth::user()->role->permission['permission']['subCategories']['list'])
                <li class="nav-item"><a href="{{ route('sub-category') }}" class="nav-link @yield('subCategory')">Sub Category</a>
                </li>
            @endisset
            @isset(Auth::user()->role->permission['permission']['subSubCategories']['list'])
            <li class="nav-item"><a href="{{ route('sub-subcategory') }}"
                    class="nav-link @yield('subSubCategory')">Sub-SubCategory</a></li>
            @endisset
        </ul>
    @endisset

   
        <a href="#" class="sl-menu-link @yield('products')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Products</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            @isset(Auth::user()->role->permission['permission']['product']['add'])
                <li class="nav-item"><a href="{{ route('add-product') }}" class="nav-link @yield('addProduct')">Add Product</a>
                @endisset
            </li>
            @isset(Auth::user()->role->permission['permission']['product']['list'])
                <li class="nav-item"><a href="{{ route('manage-product') }}" class="nav-link @yield('subCategory')">Manage
                        Product</a></li>
            @endisset
        </ul>



    @isset(Auth::user()->role->permission['permission']['coupon']['list'])
        <a href="{{ route('coupon') }}" class="sl-menu-link @yield('coupon')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Coupon</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
    @endisset

        <a href="#" class="sl-menu-link @yield('shipping')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Shipping Area</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            @isset(Auth::user()->role->permission['permission']['division']['add'])
                <li class="nav-item"><a href="{{ route('add-division') }}" class="nav-link @yield('addDivision')">Add Division</a> </li> 
            @endisset
            @isset(Auth::user()->role->permission['permission']['district']['add'])
                <li class="nav-item"><a href="{{ route('add-district') }}" class="nav-link @yield('subCategory')">Add District</a> </li>
            @endisset
            @isset(Auth::user()->role->permission['permission']['state']['add'])
             <li class="nav-item"><a href="{{ route('add-state') }}" class="nav-link @yield('subSubCategory')">Add-State</a></li>
            @endisset
        </ul>
        @isset(Auth::user()->role->permission['permission']['order']['list'])
        <a href="#" class="sl-menu-link @yield('orders')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Orders</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{ route('pending-orders') }}" class="nav-link @yield('pending-orders')">Pending
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('confirmed-orders') }}" class="nav-link @yield('confirmed-orders')">Confirmed
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('processing-orders') }}" class="nav-link @yield('processing-orders')">Processing
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('picked-orders') }}" class="nav-link @yield('picked-orders')">Picked
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('shipped-orders') }}" class="nav-link @yield('shipped-orders')">Shipped
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('delevered-orders') }}" class="nav-link @yield('delevered-orders')">Delevered
                    Orders</a></li>
            <li class="nav-item"><a href="{{ route('cancel-orders') }}" class="nav-link @yield('cancel-orders')">Cancel
                    Orders</a></li>
        </ul>
        @endisset

        @isset(Auth::user()->role->permission['permission']['report']['add'])
        <a href="{{ route('reports') }}" class="sl-menu-link @yield('reports')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Reports</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        @endisset

        @isset(Auth::user()->role->permission['permission']['review']['add'])
        <a href="{{ route('product-review-approve') }}" class="sl-menu-link @yield('reviews')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Reviews</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        @endisset

        @isset(Auth::user()->role->permission['permission']['stock']['review'])
        <a href="{{ route('product-stock') }}" class="sl-menu-link @yield('stock')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Stock</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        @endisset
        <!-------Role Management---------->
        
        <a href="#" class="sl-menu-link @yield('role')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Role Management</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{ route('role.create') }}" class="nav-link @yield('add-role')">Add
                    Role</a></li>
            <li class="nav-item"><a href="{{ route('role.index') }}" class="nav-link @yield('all-role')">All
                    Role</a></li>
        </ul>



        <a href="#" class="sl-menu-link @yield('permission')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Permission Management</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{ route('permission.create') }}" class="nav-link @yield('add-permission')">Add
                    permission</a></li>
            <li class="nav-item"><a href="{{ route('permission.index') }}" class="nav-link @yield('all-permission')">All
                    permission</a></li>
        </ul>

        <a href="#" class="sl-menu-link @yield('subadmin')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Sub Admin</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{ route('subadmin.create') }}" class="nav-link @yield('add-subadmin')">Add
                    subadmin</a></li>
            <li class="nav-item"><a href="{{ route('subadmin.index') }}" class="nav-link @yield('all-subadmin')">All
                    subadmin</a></li>
        </ul>
    </div><!-- sl-sideleft-menu -->
