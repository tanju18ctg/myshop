@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">
   
      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Edit Product</h6>
            <form action="{{ route('product-update') }}" method="post">
              @csrf
              <div class="form-layout">
                <div class="row mg-b-25">
                  <div class="col-lg-4">
                      <div class="form-group">
                          <label class="form-control-label">Select Brand<span class="tx-danger">*</span></label>
                          <select class="form-control select2" data-placeholder="Choose Browser" name="brand_id">
                              <option value="">Select Brand</option>
                              @foreach ($brands as $brand )
                                <option value="{{ $brand->id }}" {{ $brand->id == $product->brand_id ? 'selected' : '' }}>{{ $brand->brand_name_en }}</option>
                              @endforeach
                          </select>
                          @error('brand_id')
                            <span class="text-danger"> {{ $message }} </span>
                          @enderror
                      </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Select Category<span class="tx-danger">*</span></label>
                        <select class="form-control select2" data-placeholder="Choose Browser" id="category_id" name="category_id">
                            <option value="">Select Category</option>
                            @foreach ($categories as $category )
                              <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->category_name_en }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                          <span class="text-danger"> {{ $message }} </span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
               
                 <div class="col-lg-4">
                  <div class="form-group">
                      <label class="form-control-label">Select Sub Category<span class="tx-danger">*</span></label>
                      <select class="form-control select2" data-placeholder="Choose Browser" id="subcategory_id" name="subcategory_id">
                          <option value=""> </option>
                      </select>
                      @error('subcategory_id')
                        <span class="text-danger"> {{ $message }} </span>
                      @enderror
                  </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                  <div class="form-group">
                      <label class="form-control-label">Select Sub SubCategory<span class="tx-danger">*</span></label>
                      <select class="form-control select2" data-placeholder="Choose Browser" id="subsubcategory_id" name="subsubcategory_id">
                          <option value=""> </option>
                      </select>
                      @error('subsubcategory_id')
                        <span class="text-danger"> {{ $message }} </span>
                      @enderror
                  </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Name English: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_name_en" value="{{ $product->product_name_en }}" placeholder="Enter Product Name English">
                        @error('product_name_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Name Bangla: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_name_bn" value="{{ $product->product_name_bn }}" placeholder="Enter Product Name English">
                        @error('product_name_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Code : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_code" value="{{ $product->product_code }}" placeholder="Enter Product code">
                        @error('product_code')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Quantity : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_qty" value="{{ $product->product_qty }}" placeholder="Enter Product Quantity">
                        @error('product_qty')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Tags English : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_tags_en" data-role="tagsinput" value="{{ $product->product_tags_en }}" placeholder="Enter Product Tags En">
                        @error('product_tags_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Tags Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text"  data-role="tagsinput" name="product_tags_bn" value="{{ $product->product_tags_bn }}" placeholder="Enter Product Tags Bn">
                        @error('product_tags_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Size English : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-role="tagsinput" type="text" name="product_size_en" value="{{ $product->product_size_en }}" placeholder="Enter Product Size En">
                        @error('product_size_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Size Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-role="tagsinput" type="text" name="product_size_bn" value="{{ $product->product_size_bn }}" placeholder="Enter Product Size Bn">
                        @error('product_size_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Color English : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_color_en" value="{{ $product->product_color_en }}" placeholder="Enter Product Color En">
                        @error('product_color_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Color Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_color_bn" value="{{ $product->product_color_bn }}" placeholder="Enter Product Color Bn">
                        @error('product_color_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Selling Price : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="selling_price" value="{{ $product->selling_price }}" placeholder="Enter Selling Price">
                        @error('selling_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Discount Price: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="discount_price" value="{{ $product->discount_price }}" placeholder="Enter Discount Price">
                        @error('discount_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
         
                 <div class="col-lg-8">
                    <div class="form-group">
                        <label class="form-control-label">Short Description English : <span class="tx-danger">*</span></label>
                        <textarea name="short_description_en" id="summernote">{{ $product->short_description_en }}</textarea>
                        @error('short_description_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Short Description Bangla : <span class="tx-danger">*</span></label>
                        <textarea name="short_description_bn" id="summernote2">{{ $product->short_description_bn }}</textarea>
                        @error('short_description_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-6">
                    <div class="form-group">
                      <div class="form-group">
                        <label class="form-control-label">Long Description English : <span class="tx-danger">*</span></label>
                        <textarea name="long_description_en" id="summernote3">{{ $product->long_description_en }}</textarea>
                        @error('long_description_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label">Long Description Bangla : <span class="tx-danger">*</span></label>
                      <textarea name="long_description_bn" id="summernote4">{{ $product->long_description_bn }}</textarea>
                      @error('long_description_en')
                        <span class="text-danger"> {{ $message }}</span>
                      @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" name="hot_deals" value="1" {{ $product->hot_deals == 1 ? 'checked' : '' }}><span>Hot Deals</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" name="futured" value="1"><span>Futured</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" name="special_offer" value="1"><span>Special Offer</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" name="special_deals" value="1"><span>Special Deals</span>
                    </label>
                 </div><!-- col-4 -->
        


                </div><!-- row -->
    
                <div class="form-layout-footer">
                  <input type="hidden" name="id" value="{{ $product->id }}">
                  <button  type="submit" class="btn btn-success mg-r-5">Update Product</button>
                </div><!-- form-layout-footer -->

                </div>
              </div><!-- form-layout -->
            </form>

              <div class="row row-sm mt-4">
                <form action="{{ route('update-product-thumbnail') }}" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="card-group">
                        <div class="card">
                            <img src="{{ asset($product->product_thumbnail) }}" class="card-img-top" alt="image-card" style="width: 320px; height:auto">
                            <div class="card-body">
                              <h5 class="card-title">
                                <a href=""> <i class="fa fa-trash"></i></a>
                              </h5>
                                <div class="form-group">
                                  <label class="form-control-label">Change Image<span class="tx-danger">*</span></label>
                                  <input class="form-control" type="file" name="product_thumbnail" id="product_thumbnail" onchange="mainThumbUrl(this)">
                                  @error('discount_price')
                                    <span class="text-danger"> {{ $message }}</span>
                                  @enderror
                                  <img src="" id="previewThumb">
                              </div>
                            </div>
                        </div>
                  </div>
                  <div class="form-layout-footer mt-3 mb-3">
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    <input type="hidden" name="old_img" value="{{ $product->product_thumbnail }}">
                    <button  type="submit" class="btn btn-success mg-r-5">Update Product Thumbnail</button>
                  </div><!-- form-layout-footer -->
                </form>


                <form action="{{ route('update-product-image') }}" method="post" enctype="multipart/form-data">
                  @csrf
                   <div class="row row-sm"> 
                    <div class="card-group">
                      @foreach ($multiImage as $img)
                        <div class="col-md-3"> 
                          <div class="card">
                            <img src="{{ asset($img->photo_name) }}" class="card-img" alt="...">
                            <div class="card-body">
                              <h5 class="card-title">
                                <a href="{{ url('admin/multiImage-delete/'.$img->id) }}"> <i class="fa fa-trash"></i></a>
                              </h5>
                                <div class="form-group">
                                  <label class="form-control-label">Change Image<span class="tx-danger">*</span></label>
                                  <input class="form-control" type="file" name="multiImg[{{ $img->id }}]" multiple id="multiImage" onchange="multiImgUrl(this)>
                                  @error('discount_price')
                                    <span class="text-danger"> {{ $message }}</span>
                                  @enderror
                                  <img src="" id="previewImg">
                              </div>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </div>
                   </div>
                  <div class="form-layout-footer mt-3">
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    <button  type="submit" class="btn btn-success mg-r-5">Update Multiple Image</button>
                  </div><!-- form-layout-footer -->
                </form>
              </div>

      </div><!-- card -->


          {{-- <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label"> Main Thumbnail<span class="tx-danger">*</span></label>
                <input class="form-control" type="file" name="thumbnail" onchange="mainThumbUrl(this)">
                @error('discount_price')
                  <span class="text-danger"> {{ $message }}</span>
                @enderror
                <img src="" id="previewThumb">
            </div>
         </div><!-- col-4 -->
         <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Multiple Image<span class="tx-danger">*</span></label>
                <input class="form-control" type="file" name="multiple[]" multiple id="multiImage">
                @error('discount_price')
                  <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>
            <div class="row" id="previewImage"> 

            </div>
         </div><!-- col-4 --> --}}
    </div>
    </div>


    <script src="{{ asset('backend/lib/jquery/jquery.js') }}"> </script>

<script type="text/javascript"> 
  $(document).ready(function () {
    $('#category_id').on('change', function(){
          var cat_id = $(this).val();
           if(cat_id)
           {
              $.ajax({
                url : "{{ url('/admin/subcategory/ajax') }}/"+cat_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                  $('#subsubcategory_id').html('');
                   var d = $('#subcategory_id').empty();
                   $.each(data, function(key, value){
                      $('#subcategory_id').append('<option value="'+value.id+'">'+ value.subcategory_name_en + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })


    $('#subcategory_id').on('change', function(){
          var subcat_id = $(this).val();
           if(subcat_id)
           {
              $.ajax({
                url : "{{ url('/admin/subsubcategory/ajax') }}/"+subcat_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                   var d = $('#subsubcategory_id').empty();
                   $.each(data, function(key, value){
                      $('#subsubcategory_id').append('<option value="'+value.id+'">'+ value.subsubCategory_name_en + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })
  })
</script>


<!-------multiImage Preview-------------> 
<script> 
  $(document).ready(function(){
    $("#multiImage").on('change', function(){
      if(window.File && window.FileReader && window.FileList && window.Blob)
      {
        var data = $(this)[0].files;
        $.each(data, function(index, file){
          if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type))
          {
            var fRead = new FileReader();
            fRead.onload = (function (file) 
            {
              return function (e)
              {
                var img = $('<img/>').addClass('thumb').attr('src', e.target.result).width(80).height(80); //create image element 
                $("#previewImage").append(img); //append image to output element 
              };
            })(file);
            fRead.readAsDataURL(file); //URL Representing URl Data
          }
        })
      }else {
        alert("your browser doesn't support this api");
      }
    })
  })
</script>

//main Thumb

<script>
  function mainThumbUrl(input){
    if(input.files && input.files[0])
    {
      var fReader = new FileReader();
      fReader.onload = function (e) 
      {
        $('#previewThumb').attr('src', e.target.result).width(80).height(80);
      };
      fReader.readAsDataURL(input.files[0]);
    }
  }
</script>

//main Thumb
<script>
  function mainThumbUrl(input){
    if(input.files && input.files[0])
    {
      var fReader = new FileReader();
      fReader.onload = function (e) 
      {
        $('#previewThumb').attr('src', e.target.result).width(80).height(80);
      };
      fReader.readAsDataURL(input.files[0]);
    }
  }
  function multiImgUrl(input){
    if(input.files && input.files[0])
    {
      var fReader = new FileReader();
      fReader.onload = function (e) 
      {
        $('#previewImg').attr('src', e.target.result).width(80).height(80);
      };
      fReader.readAsDataURL(input.files[0]);
    }
  }
</script>
@endsection