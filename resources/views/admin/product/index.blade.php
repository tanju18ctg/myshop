@extends('layouts.admin-master')

@section('products')
    active show-sub
@endsection

@section('manage-product')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Products </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-12"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Product List</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-wrapper">
                          <table id="datatable1" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                <th class="wd-15p">Product Img</th>
                                <th class="wd-15p">Product Name En</th>
                                <th class="wd-15p">Product Price</th>
                                <th class="wd-15p">Product Qty</th>
                                <th class="wd-15p">Discount</th>
                                <th class="wd-10p"> Status </th>
                                <th class="wd-15p">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($products as $item )
                                <tr>
                                    <td> <img src="{{ asset($item->product_thumbnail) }}" alt=""> </td>
                                    <td>{{ $item->product_name_en }}</td>
                                    <td>{{ $item->selling_price }}৳</td>
                                    <td>{{ $item->product_qty }}</td>
                                    <td>
                                      @if ( $item->discount_price == null)
                                        <span class="badge badge-fill badge-danger"> NO </span>
                                        @else
                                          @php
                                            $discountAmount = $item->selling_price - $item->discount_price;
                                            $discountParcent = ($item->discount_price / $discountAmount)*100; 
                                          @endphp
                                          <span class="badge badge-fill badge-primary"> {{ round($discountParcent)}}% </span>
                                      @endif
                                    </td>
                                    <td>
                                      @if ($item->status == 1)
                                      <span class="badge badge-pill badge-primary">Active</span>
                                      @else
                                      <span class="badge badge-pill badge-danger">Inactive</span>

                                      @endif
                                    </td>
                                    <td>
                                      <a href="{{ url('admin/product-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-eye"> </i></a>
                                      <a href="{{ url('admin/product-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-pencil"> </i></a>
                                      <a href="{{ url('admin/product-delete/'.$item->id) }}" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-trash"> </i> </a>
                                      <a href="{{ url('admin/product-inactive/'.$item->id) }}" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-arrow-down"> </i> </a>
                                      <a href="{{ url('admin/product-active/'.$item->id) }}" class="btn btn-primary btn-sm" id="delete"><i class="fa fa-arrow-up"> </i> </a>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

              
            </div> 

        </div>
    </div>
@endsection