@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">
   
      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Add Product</h6>
            <form action="{{ route('product-store') }}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-layout">
                <div class="row mg-b-25">
                  <div class="col-lg-4">
                      <div class="form-group">
                          <label class="form-control-label">Select Brand<span class="tx-danger">*</span></label>
                          <select class="form-control select2" data-placeholder="Choose Browser" name="brand_id">
                              <option value="">Select Brand</option>
                              @foreach ($brands as $brand )
                                <option value="{{ $brand->id }}">{{ $brand->brand_name_en }}</option>
                              @endforeach
                          </select>
                          @error('brand_id')
                            <span class="text-danger"> {{ $message }} </span>
                          @enderror
                      </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Select Category<span class="tx-danger">*</span></label>
                        <select class="form-control select2" data-placeholder="Choose Browser" id="category_id" name="category_id">
                            <option value="">Select Category</option>
                            @foreach ($categories as $category )
                              <option value="{{ $category->id }}">{{ $category->category_name_en }}</option>
                            @endforeach
                        </select>
                        @error('category_id')
                          <span class="text-danger"> {{ $message }} </span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
               
                 <div class="col-lg-4">
                  <div class="form-group">
                      <label class="form-control-label">Select Sub Category<span class="tx-danger">*</span></label>
                      <select class="form-control select2" data-placeholder="Choose Browser" id="subcategory_id" name="subcategory_id">
                          <option value=""> </option>
                      </select>
                      @error('subcategory_id')
                        <span class="text-danger"> {{ $message }} </span>
                      @enderror
                  </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                  <div class="form-group">
                      <label class="form-control-label">Select Sub SubCategory<span class="tx-danger">*</span></label>
                      <select class="form-control select2" data-placeholder="Choose Browser" id="subsubcategory_id" name="subsubcategory_id">
                          <option value=""> </option>
                      </select>
                      @error('subsubcategory_id')
                        <span class="text-danger"> {{ $message }} </span>
                      @enderror
                  </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Name English: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_name_en" value="{{ old('product_name_en') }}" placeholder="Enter Product Name English">
                        @error('product_name_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Name Bangla: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_name_bn" value="{{ old('product_name_bn') }}" placeholder="Enter Product Name English">
                        @error('product_name_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Code : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_code" value="{{ old('product_code') }}" placeholder="Enter Product code">
                        @error('product_code')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Quantity : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_qty" value="{{ old('product_qty') }}" placeholder="Enter Product Quantity">
                        @error('product_qty')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Tags English : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_tags_en" data-role="tagsinput" value="{{ old('product_tags_en') }}" placeholder="Enter Product Tags En">
                        @error('product_tags_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Tags Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text"  data-role="tagsinput" name="product_tags_bn" value="{{ old('product_tags_bn') }}" placeholder="Enter Product Tags Bn">
                        @error('product_tags_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Size English : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-role="tagsinput" type="text" name="product_size_en" value="{{ old('product_size_en') }}" placeholder="Enter Product Size En">
                        @error('product_size_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Size Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" data-role="tagsinput" type="text" name="product_size_bn" value="{{ old('product_size_bn') }}" placeholder="Enter Product Size Bn">
                        @error('product_size_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Color English : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_color_en" value="{{ old('product_color_en') }}" placeholder="Enter Product Color En">
                        @error('product_color_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Product Color Bangla : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="product_color_bn" value="{{ old('product_color_bn') }}" placeholder="Enter Product Color Bn">
                        @error('product_color_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Selling Price : <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="selling_price" value="{{ old('selling_price') }}" placeholder="Enter Selling Price">
                        @error('selling_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Discount Price: <span class="tx-danger">*</span></label>
                        <input class="form-control" type="text" name="discount_price" value="{{ old('discount_price') }}" placeholder="Enter Discount Price">
                        @error('discount_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label"> Main Thumbnail<span class="tx-danger">*</span></label>
                        <input class="form-control" type="file" name="thumbnail" onchange="mainThumbUrl(this)">
                        @error('discount_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                        <img src="" id="previewThumb">
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Multiple Image<span class="tx-danger">*</span></label>
                        <input class="form-control" type="file" name="multiple[]" multiple id="multiImage">
                        @error('discount_price')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row" id="previewImage"> 

                    </div>
                 </div><!-- col-4 -->
                
                 <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Short Description English : <span class="tx-danger">*</span></label>
                        <textarea name="short_description_en" id="summernote"></textarea>
                        @error('short_description_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Short Description Bangla : <span class="tx-danger">*</span></label>
                        <textarea name="short_description_bn" id="summernote2"></textarea>
                        @error('short_description_bn')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                 </div><!-- col-4 -->

                 <div class="col-lg-6">
                    <div class="form-group">
                      <div class="form-group">
                        <label class="form-control-label">Long Description English : <span class="tx-danger">*</span></label>
                        <textarea name="long_description_en" id="summernote3"></textarea>
                        @error('long_description_en')
                          <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label">Long Description Bangla : <span class="tx-danger">*</span></label>
                      <textarea name="long_description_bn" id="summernote4"></textarea>
                      @error('long_description_en')
                        <span class="text-danger"> {{ $message }}</span>
                      @enderror
                    </div>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" checked name="hot_deals" value="1"><span>Hot Deals</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" checked name="futured" value="1"><span>Futured</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" checked name="special_offer" value="1"><span>Special Offer</span>
                    </label>
                 </div><!-- col-4 -->
                 <div class="col-lg-3">
                    <label class="ckbox">
                      <input type="checkbox" checked name="special_deals" value="1"><span>Special Deals</span>
                    </label>
                 </div><!-- col-4 -->
        


                </div><!-- row -->
    
                <div class="form-layout-footer">
                  <input type="hidden" name="id" value="">
                  <button  type="submit" class="btn btn-success mg-r-5">Add Product</button>

                </div><!-- form-layout-footer -->
              </div><!-- form-layout -->
            </form>
          </div><!-- card -->

    </div>
    </div>


    <script src="{{ asset('backend/lib/jquery/jquery.js') }}"> </script>

<script type="text/javascript"> 
  $(document).ready(function () {
    $('#category_id').on('change', function(){
          var cat_id = $(this).val();
           if(cat_id)
           {
              $.ajax({
                url : "{{ url('/admin/subcategory/ajax') }}/"+cat_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                  $('#subsubcategory_id').html('');
                   var d = $('#subcategory_id').empty();
                   $.each(data, function(key, value){
                      $('#subcategory_id').append('<option value="'+value.id+'">'+ value.subcategory_name_en + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })


    $('#subcategory_id').on('change', function(){
          var subcat_id = $(this).val();
           if(subcat_id)
           {
              $.ajax({
                url : "{{ url('/admin/subsubcategory/ajax') }}/"+subcat_id,
                type : 'GET',
                dataType : 'json',
                success : function(data){
                   var d = $('#subsubcategory_id').empty();
                   $.each(data, function(key, value){
                      $('#subsubcategory_id').append('<option value="'+value.id+'">'+ value.subsubCategory_name_en + '</option>');
                   })
                },
    
              });
           }else{
             alert('danger');
           }
    })
  })
</script>


<!-------multiImage Preview-------------> 
<script> 
  $(document).ready(function(){
    $("#multiImage").on('change', function(){
      if(window.File && window.FileReader && window.FileList && window.Blob)
      {
        var data = $(this)[0].files;
        $.each(data, function(index, file){
          if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type))
          {
            var fRead = new FileReader();
            fRead.onload = (function (file) 
            {
              return function (e)
              {
                var img = $('<img/>').addClass('thumb').attr('src', e.target.result).width(80).height(80); //create image element 
                $("#previewImage").append(img); //append image to output element 
              };
            })(file);
            fRead.readAsDataURL(file); //URL Representing URl Data
          }
        })
      }else {
        alert("your browser doesn't support this api");
      }
    })
  })
</script>

//main Thumb
<script>
  function mainThumbUrl(input){
    if(input.files && input.files[0])
    {
      var fReader = new FileReader();
      fReader.onload = function (e) 
      {
        $('#previewThumb').attr('src', e.target.result).width(80).height(80);
      };
      fReader.readAsDataURL(input.files[0]);
    }
  }
</script>
@endsection