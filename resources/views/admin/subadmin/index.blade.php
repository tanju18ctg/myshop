@extends('layouts.admin-master')

@section('permission')
    active show-sub
@endsection

@section('all-permission')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> permissions </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-10 m-auto"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-wrapper">
                          <table id="datatable1" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                <th class="wd-25p">Sl No</th>
                                <th class="wd-25p"> Name</th>
                                <th class="wd-25p">Email</th>
                                <th class="wd-25p">Role</th>
                                <th class="wd-25p">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($users as $user )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                      <span class="badge badge-pill badge-success">{{ $user->role->name }}</span>
                                    </td>
                                    <td>
                                      <a href="{{ route('subadmin.edit', $user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"> </i></a>
                                      <form action="{{ route('subadmin.destroy', $user->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm"> <i class="fa fa-trash"> </i> </button>
                                      </form>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

            </div> 

        </div>
    </div>
@endsection