@extends('layouts.admin-master')

@section('coupon')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Coupon </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <!---------Add-create-page--------> 
                <div class="col-md-8 m-auto"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Edit Coupon</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('coupon-update')}}" method="post"> 
                            @csrf
                            <div class="form-group">
                              <label class="form-control-label">Coupon Name  <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="coupon_name" value="{{ $coupon->coupon_name }}" placeholder="Enter Brand Name English">
                              @error('coupon_name')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>
                          <div class="form-group">
                              <label class="form-control-label">Coupon Discount % <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="coupon_discount" value="{{ $coupon->coupon_discount}}" placeholder="Enter Brand Name Bangla">
                              @error('coupon_discount')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Coupon Validaty <span class="tx-danger">*</span></label>
                                <input class="form-control" type="date" name="coupon_validaty" value="{{ $coupon->coupon_validaty }}" placeholder="Enter Brand Name Bangla" min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                @error('coupon_validaty')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                              </div>
                          
                            <div class="form-layout-footer">
                                <input type="hidden" name="id" value="{{ $coupon->id }}">
                              <button class="btn btn-info mg-r-5" type="submit">Update Coupon</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection