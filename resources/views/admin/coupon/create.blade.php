@extends('layouts.admin-master')

@section('coupon')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Coupon </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-8"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-wrapper">
                          <table id="datatable1" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                <th class="wd-25p">Coupon Name</th>
                                <th class="wd-25p">Coupon Discount</th>
                                <th class="wd-25p">Coupon Validaty</th>
                                <th class="wd-25p">Status</th>
                                <th class="wd-25p">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($coupons as $item )
                                <tr>
                                    <td> {{ $item->coupon_name }} </td>
                                    <td>{{ $item->coupon_discount }}</td>
                                    <td>{{ Carbon\Carbon::parse($item->coupon_validaty)->format('D, d f Y') }}</td>
                                    <td> @if ($item->coupon_validaty >= Carbon\Carbon::now()->format('Y-m-d'))
                                        <span class="badge badge-pill badge-primary">Valid</span>
                                        @else
                                        <span class="badge badge-pill badge-danger">Invalid</span>
  
                                        @endif
                                    </td>
                                    <td>
                                      <a href="{{ route('coupon-edit', $item->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"> </i></a>
                                      <a href="{{ route('coupon-delete', $item->id) }}" class="btn btn-danger btn-sm"> <i class="fa fa-trash"> </i> </a>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

                <!---------Add-create-page--------> 
                <div class="col-md-4"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Coupon</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('coupon-store')}}" method="post"> 
                            @csrf
                            <div class="form-group">
                              <label class="form-control-label">Coupon Name  <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="coupon_name" value="{{ old('coupon_name') }}" placeholder="Enter Brand Name English">
                              @error('coupon_name')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>
                          <div class="form-group">
                              <label class="form-control-label">Coupon Discount % <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="coupon_discount" value="{{ old('coupon_discount') }}" placeholder="Enter Brand Name Bangla">
                              @error('coupon_discount')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Coupon Validaty <span class="tx-danger">*</span></label>
                                <input class="form-control" type="date" name="coupon_validaty" value="{{ old('coupon_validaty') }}" placeholder="Enter Brand Name Bangla" min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                @error('coupon_validaty')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                              </div>

                              <div class="form-group">
                                <label class="form-control-label">Coupon Status <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="status" value="{{ old('status') }}" placeholder="Enter Brand Name Bangla">
                                @error('status')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                              </div>
                
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Add Coupon</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection