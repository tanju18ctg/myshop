@extends('layouts.admin-master')

@section('reports')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Reports </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-12 m-auto">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr style="background: #181819;">
                                    <td class="col-md-3 text-center">
                                        <label for="">Date</label>
                                    </td>

                                    <td class="col-md-3 text-center">
                                        <label for="">Invoice</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Amount</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Tnx Id</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Staus</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Action</label>
                                    </td>

                                </tr>

                                @foreach ($orders as $item)
                                    <tr>
                                        <td class="col-md-3 text-center">
                                            <div class="product-name">
                                                <strong>{{ $item->order_date }}</strong>
                                            </div>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->invoice_no }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->amount }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->transaction_id }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->status }}</strong>
                                        </td>

                                        <td class="col-md-2">
                                            <a href="{{ url('user/order-view/' . $item->id) }}"
                                                class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>

                                            <a href="{{ url('user/generate-pdf/' . $item->id) }}"
                                                class="btn btn-sm btn-danger "><i class="fa fa-download"
                                                    style="color:white;"></i></a>

                                        </td>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
