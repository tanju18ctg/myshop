@extends('layouts.admin-master')

@section('reports')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Reports </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">

                <!---------Add-create-page-------->
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5> Search By Date </h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('search-date') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="form-control-label">Select Date <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="date" name="date" value="{{ old('date') }}">
                                    @error('date')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button class="btn btn-info mg-r-5" type="submit"> <i class="fa fa-search"></i>
                                        Search </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5> Search By Month </h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('search-month') }}" method="post">
                                @csrf

                                <div class="form-group">
                                    <label class="form-control-label">Select Month <span class="tx-danger">*</span></label>
                                    <select name="month" id="" class="form-control select2">
                                        <option hidden>Select Month</option>
                                        <option value="January">January</option>
                                        <option value="February">February</option>
                                        <option value="March">March</option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="October">October</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                    </select>
                                    @error('month')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Select Year <span class="tx-danger">*</span></label>
                                    <select name="year" id="" class="form-control select2">
                                        <option hidden>Select Year</option>
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                    </select>
                                    @error('year')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button class="btn btn-info mg-r-5" type="submit"> <i class="fa fa-search"></i>
                                        Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5> Search By Year </h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('search-year') }}" method="post">
                                @csrf

                                <div class="form-group">
                                    <label class="form-control-label">Select Year <span class="tx-danger">*</span></label>
                                    <select name="year_name" id="" class="form-control select-2">
                                        <option hidden>Select year</option>
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                    </select>
                                    @error('year_name')
                                        <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button class="btn btn-info mg-r-5" type="submit"> <i class="fa fa-search"></i>
                                        Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
