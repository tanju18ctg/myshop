@extends('layouts.admin-master')

@section('shipping')
    active show-sub
@endsection

@section('addDivision')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Shipping District </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
    
                <!---------Add-create-page--------> 
                <div class="col-md-8 m-auto"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Division</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('update-district')}}" method="post"> 
                            @csrf

                            <div class="form-group">
                              <label class="form-control-label">Select Division<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" name="division_id">
                                  <option value="">Select Division</option>
                                  @foreach ($divisions as $division )
                                    <option value="{{ $division->id }}" {{ $division->id == $district->division_id ? 'selected' : '' }}>{{ $division->division_name }}</option>
                                  @endforeach
                              </select>
                              @error('division_id')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">District Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="district_name" value="{{ $district->district_name }}" placeholder="Enter District Name">
                                @error('district_name')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Update District</button>
                              <input type="hidden" name="id" value="{{ $district->id }}">
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection