@extends('layouts.admin-master')

@section('shipping')
    active show-sub
@endsection

@section('addDivision')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Shipping District </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-8"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-wrapper">
                          <table id="datatable1" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                <th class="wd-25p">Division Name</th>
                                <th class="wd-25p">District Name</th>
                                <th class="wd-25p">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($districts as $item )
                                <tr>
                                    <td> {{ $item->division->division_name }} </td>
                                    <td> {{ $item->district_name }} </td>
                                    <td>
                                      <a href="{{ url('admin/district-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-pencil"> </i></a>
                                      <a href="{{ url('admin/district-delete/'.$item->id) }}" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-trash"> </i> </a>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

                <!---------Add-create-page--------> 
                <div class="col-md-4"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Division</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('store-district')}}" method="post"> 
                            @csrf

                            <div class="form-group">
                              <label class="form-control-label">Select Division<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" name="division_id">
                                  <option value="">Select Division</option>
                                  @foreach ($divisions as $item )
                                    <option value="{{ $item->id }}">{{ $item->division_name }}</option>
                                  @endforeach
                              </select>
                              @error('division_id')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">District Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="district_name" value="{{ old('district_name') }}" placeholder="Enter Division Name">
                                @error('district_name')district_name
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Add District</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection