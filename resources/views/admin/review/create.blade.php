@extends('layouts.admin-master')

@section('reviews')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Reviews List </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page------->
                <div class="col-md-12">
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to
                            the table, as shown in this example.</p>

                        <div class="table-responsive">
                            <table id="datatable1" class="table">
                                <thead>
                                    <tr>
                                        <th class="wd-20p">Product Image</th>
                                        <th class="wd-20p">Customer Name</th>
                                        <th class="wd-20p">Product Name</th>
                                        <th class="wd-20p">Comment</th>
                                        <th class="wd-10p">Status</th>
                                        <th class="wd-10p">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reviews as $review)
                                        <tr>
                                            <td> <img src="{{ asset($review->product->product_thumbnail) }}" alt="brand"
                                                    style="width:80px;"> </td>
                                            <td>{{ $review->user->name }}</td>
                                            <td>{{ $review->product->product_name_en }}</td>
                                            <td>{{ $review->comment }}</td>
                                            <td>
                                                <span class="badge badge-pill badge-primary">{{ $review->status }}</span>
                                                @if ($review->status == 'pending')
                                                    <a href="{{ url('admin/review-approve/'.$review->id) }}" class="btn btn-sm btn-primary">Approve Now</a>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('review-delete', $review->id) }}"
                                                    class="btn btn-danger btn-sm"> <i class="fa fa-trash"> </i> </a>
                                            </td>
                                        </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div><!-- table-wrapper -->
                    </div><!-- card -->
                </div>
            </div>

        </div>
    </div>
@endsection
