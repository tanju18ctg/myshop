@extends('layouts.admin-master')

@section('categories')
    active show-sub
@endsection
@section('subCategory')
    active show-sub
@endsection

@section('sub-categories')
    active
@endsection

@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Sub Categoreis </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-8"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-wrapper">
                          <table id="datatable1" class="table display responsive nowrap">
                            <thead>
                              <tr>
                                <th class="wd-25p"> Category Name</th>
                                <th class="wd-25p"> Sub Category Name En </th>
                                <th class="wd-25p"> Sub Category Name Bn </th>
                                <th class="wd-25p"> Action </th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($subCategories as $item )
                                <tr>
                                    <td> {{ $item->category->category_name_en}} </td>
                                    <td>{{ $item->subcategory_name_en }}</td>
                                    <td>{{ $item->subcategory_name_bn }}</td>
                                    <td>
                                      <a href="{{ url('admin/subcategory-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-pencil"> </i></a>
                                      <a href="{{ url('admin/subcategory-delete/'.$item->id) }}" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-trash"> </i> </a>
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

                <!---------Add-create-page--------> 
                <div class="col-md-4"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Sub Category</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('subcategory-store')}}" method="post"> 
                            @csrf
                            <div class="form-group">
                              <label class="form-control-label">Select Category<span class="tx-danger">*</span></label>
                              <select class="form-control select2" data-placeholder="Choose Browser" name="category_id">
                                  <option value="">Select Category</option>
                                  @foreach ($categories as $item )
                                    <option value="{{ $item->id }}">{{ $item->category_name_en }}</option>
                                  @endforeach
                              </select>
                              @error('categorie_name')
                                <span class="text-danger"> {{ $message }} </span>
                              @enderror
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Subcategory Name English: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="subcategory_name_en" value="{{ old('subcategory_name_en') }}" placeholder="Enter subCategory Name English">
                                @error('subcategory_name_en')
                                  <span class="text-danger"> {{ $message }}</span>
                                @enderror
                              </div>

                          <div class="form-group">
                              <label class="form-control-label">Subcategory Name Bangla: <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="subcategory_name_bn" value="{{ old('subcategory_name_bn') }}" placeholder="Enter subCategory Name Bangla">
                              @error('subcategory_name_bn')
                                <span class="text-danger"> {{ $message }}</span>
                              @enderror
                            </div>
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Save </button>
                            </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>

    <script>
        $(function(){
          'use strict';
  
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
        })
      </script>
@endsection