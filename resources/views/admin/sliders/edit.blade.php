@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">
   
        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Update Slider</h6>
                <form action="{{ route('slider-update') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-layout">
                    <div class="row mg-b-25">

                        @if ($slider->title_en == NULL)
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Slider Image: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="file" name="image">
                            </div>
                        </div><!-- col-4 -->
                          <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Old Image: <span class="tx-danger">*</span></label>
                               <img src="{{ asset($slider->image) }}" alt="slider" height="80px" width="180px">
                            </div>
                          </div><!-- col-4 -->
                           @else

                      <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Slider Title English <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title_en" value="{{ $slider->title_en }}" placeholder="Enter Slider Title">
                          </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                         <div class="form-group">
                            <label class="form-control-label">Slider Title Bangla <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title_bn" value="{{ $slider->title_bn  }}" placeholder="Enter Slider Title">
                          </div>
                      </div><!-- col-4 -->
                   
                    
                      <div class="col-lg-4">
                          <div class="form-group">
                            <label class="form-control-label">Slider Description English: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="description_en" value="{{ $slider->description_en  }}" placeholder="Enter Slider Description English">
                          </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Slider Description Bangla: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="description_bn" value="{{ $slider->description_bn  }}" placeholder="Enter Slider Description Bangla">
                          </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Slider Image: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="file" name="image">
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Old Image: <span class="tx-danger">*</span></label>
                           <img src="{{ asset($slider->image) }}" alt="slider" height="80px" width="180px">
                        </div>
                      </div><!-- col-4 -->

                      @endif


                    </div><!-- row -->
        
                    <div class="form-layout-footer">
                      <input type="hidden" name="id" value="{{ $slider->id }}">
                      <input type="hidden" name="old_image" value="{{ $slider->image }}">
                      <button  type="submit" class="btn btn-success mg-r-5">Update</button>
  
                    </div><!-- form-layout-footer -->
                  </div><!-- form-layout -->
                </form>
              </div><!-- card -->

        </div>
    </div>
@endsection