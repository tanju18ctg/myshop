@extends('layouts.admin-master')

@section('sliders')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item"> 
                Shop Mama </a>
         <span class="breadcrumb-item active"> Sliders </span>
        </nav> 
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page-------> 
                <div class="col-md-8"> 
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
              
                        <div class="table-responsive-sm">
                          <table id="datatable1" class="table table-striped">
                            <thead>
                              <tr>
                                <th class="wd-20p">Slider Image</th>
                                <th class="wd-15p">Slider Title</th>
                                <th class="wd-15p">Description</th>
                                <th class="wd-15p">Status</th>
                                <th class="wd-35p">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($sliders as $item )
                                <tr>
                                    <td> <img src="{{ asset($item->image)}}" alt="brand" style="width:80px;"> </td>
                                    <td>{{ $item->title_en }}</td>
                                    <td>{{ $item->description_en }}</td>
                                    <td>  
                                      @if ($item->status == 1)
                                      <span class="badge badge-pill badge-primary">Active</span>
                                      @else
                                      <span class="badge badge-pill badge-danger">Inactive</span>
                                      @endif
                                    </td>
                                    <td>
                                      <a href="{{ url('admin/slider-edit/'.$item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-pencil"> </i></a>
                                      <a href="{{ url('admin/slider-delete/'.$item->id) }}" class="btn btn-danger btn-sm"> <i class="fa fa-trash"> </i></a>
                                      <a href="{{ url('admin/slider-inactive/'.$item->id) }}" title="Inactive" class="btn btn-danger btn-sm"> <i class="fa fa-arrow-down"> </i></a>
                                      <a href="{{ url('admin/slider-active/'.$item->id) }}" title="Active" class="btn btn-success btn-sm"> <i class="fa fa-arrow-up"> </i></a>
                                      
                                    </td>
                                </tr>
                              @endforeach
                            
                              
                            </tbody>
                          </table>
                        </div><!-- table-wrapper -->
                      </div><!-- card -->
                </div>

                <!---------Add-create-page--------> 
                <div class="col-md-4"> 
                    <div class="card"> 
                        <div class="card-header"> 
                            <h5> Add New Slider</h5>
                        </div>
                        <div class="card-body"> 
                          <form action="{{route('slider-store')}}" method="post" enctype="multipart/form-data"> 
                            @csrf
                            <div class="form-group">
                              <label class="form-control-label">Slider Title English <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="title_en" value="{{ old('title_en') }}" placeholder="Enter Slider Title">
                            </div>
                            <div class="form-group">
                              <label class="form-control-label">Slider Title Bangla <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="title_bn" value="{{ old('title_bn') }}" placeholder="Enter Slider Title">
                            </div>
                          <div class="form-group">
                              <label class="form-control-label">Slider Description English: <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="description_en" value="{{ old('description_en') }}" placeholder="Enter Slider Description English">
                            </div>
                          <div class="form-group">
                              <label class="form-control-label">Slider Description Bangla: <span class="tx-danger">*</span></label>
                              <input class="form-control" type="text" name="description_bn" value="{{ old('description_bn') }}" placeholder="Enter Slider Description Bangla">
                            </div>
                          <div class="form-group">
                              <label class="form-control-label">Slider Image: <span class="tx-danger">*</span></label>
                              <input class="form-control" type="file" name="image">
                          </div>
                          
                            <div class="form-layout-footer">
                              <button class="btn btn-info mg-r-5" type="submit">Add Slider</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div> 

        </div>
    </div>
@endsection