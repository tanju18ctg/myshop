@extends('layouts.admin-master')

@section('admin-content')
    <div class="sl-mainpanel">
   
        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">Update Brand</h6>
                <form action="{{ route('brand-update') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-layout">
                    <div class="row mg-b-25">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Brand Name En: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="brand_name_en" value="{{ $brand->brand_name_en }}" placeholder="Enter firstname">
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Brand Name Bn: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="brand_name_bn" value="{{ $brand->brand_name_bn }}" placeholder="Enter lastname">
                        </div>
                      </div><!-- col-4 -->
                   
                    
                      <div class="col-lg-4">
                        <div class="form-group mg-b-10-force">
                          <img src="{{ asset($brand->brand_image) }}" alt="brand" style="width:120px">
                          <input type="hidden" name="old_image" value="{{ $brand->brand_image }}">
                          <input type="file" name="brand_image" id="">
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row -->
        
                    <div class="form-layout-footer">
                      <input type="hidden" name="id" value="{{ $brand->id }}">
                      <button  type="submit" class="btn btn-success mg-r-5">Update</button>
  
                    </div><!-- form-layout-footer -->
                  </div><!-- form-layout -->
                </form>
              </div><!-- card -->

        </div>
    </div>
@endsection