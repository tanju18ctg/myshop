@extends('layouts.admin-master')

@section('orders')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> Pending Orders </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page------->
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item active" aria-current="true">Shipping Address</li>
                        <li class="list-group-item">
                            <strong> Shipping Name : </strong> {{ $order->name }}
                        </li>
                        <li class="list-group-item">
                            <strong> Shipping Phone : </strong> {{ $order->phone }}
                        </li>
                        <li class="list-group-item">
                            <strong> Shipping Email: </strong> {{ $order->email }}
                        </li>
                        <li class="list-group-item">
                            <strong> Division : </strong> {{ $order->division->division_name }}
                        </li>
                        <li class="list-group-item">
                            <strong> District : </strong> {{ $order->district->district_name }}
                        </li>
                        <li class="list-group-item">
                            <strong> State : </strong> {{ $order->state->state_name }}
                        </li>
                        <li class="list-group-item">
                            <strong> Post Code : </strong> {{ $order->post_code }}
                        </li>
                        <li class="list-group-item">
                            <strong> Order Date: </strong> {{ $order->order_date }}
                        </li>
                    </ul>
                </div>

                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item active" aria-current="true">Order Details</li>
                        <li class="list-group-item">
                            <strong>Name :</strong> {{ $order->user->name }}
                        </li>
                        <li class="list-group-item">
                            <strong>Phone :</strong> {{ $order->user->phone }}
                        </li>
                        <li class="list-group-item">
                            <strong>Email :</strong> {{ $order->user->email }}
                        </li>
                        <li class="list-group-item">
                            <strong>Payment By :</strong> {{ $order->payment_method }}
                        </li>
                        <li class="list-group-item">
                            <strong>Tnx Id :</strong> {{ $order->transaction_id }}
                        </li>
                        <li class="list-group-item">
                            <strong>Invice No :</strong> {{ $order->invoice_no }}
                        </li>
                        <li class="list-group-item">
                            <strong>Order Total:</strong> {{ $order->amount }}
                        </li>
                        <li class="list-group-item">
                            <strong>Order Status : </strong> <span class="badge badge-pill badge-primary">
                                {{ $order->status }} </span>
                        </li>
                        @if ($order->status == 'pending')
                            <li class="list-group-item">
                                <a href="{{ url('admin/pending-to-confirm/' . $order->id) }}"
                                    class="btn btn-block btn-success" id="confirm">Confirm Order</a>
                                <a href="{{ url('admin/pending-to-cancel/' . $order->id) }}"
                                    class="btn btn-block btn-danger" id="cancel">Cancel Order</a>
                            </li>
                        @elseif($order->status == 'confirmed')
                            <a href="{{ url('admin/confirm-to-processing/' . $order->id) }}"
                                class="btn btn-block btn-success" id="processing">Processing Order</a>
                        @elseif($order->status == 'processing')
                            <a href="{{ url('admin/processing-to-picked/' . $order->id) }}" class="btn btn-block btn-success"
                                id="picked">Picked Order</a>
                        @elseif($order->status == 'picked')
                            <a href="{{ url('admin/picked-to-shipped/' . $order->id) }}" class="btn btn-block btn-success"
                                id="shipped">Shipped Order</a>
                        @elseif($order->status == 'shipped')
                            <a href="{{ url('admin/shipped-to-delevered/' . $order->id) }}" class="btn btn-block btn-success"
                                id="delevered">Delevered Order</a>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-9 m-auto">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr style="background: #181819;">
                                    <td class="col-md-1 text-center">
                                        <label for="">Image</label>
                                    </td>
                                    <td class="col-md-3 text-center">
                                        <label for="">Poduct Name</label>
                                    </td>

                                    <td class="col-md-3 text-center">
                                        <label for="">Poduct Code</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Color</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Size</label>
                                    </td>

                                    <td class="col-md-2 text-center">
                                        <label for="">Quantity</label>
                                    </td>

                                    <td class="col-md-1 text-center">
                                        <label for="">Price</label>
                                    </td>
                                    <td class="col-md-1 text-center">
                                        <label for="">Review</label>
                                    </td>

                                </tr>

                                @foreach ($orderItems as $item)
                                    <tr>
                                        <td class="col-md-1 text-center"><img
                                                src="{{ asset($item->product->product_thumbnail) }}" height="50px;"
                                                width="50px;" alt="imga"></td>
                                        <td class="col-md-3 text-center">
                                            <div class="product-name">
                                                <strong>{{ $item->product->product_name_en }}</strong>
                                            </div>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->product->product_code }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->color }}</strong>
                                        </td>

                                        <td class="col-md-2 text-center">
                                            @if ($item->size == null)
                                                ---
                                            @else
                                                <strong>{{ $item->size }}</strong>
                                            @endif
                                        </td>

                                        <td class="col-md-2 text-center">
                                            <strong>{{ $item->qty }}</strong>
                                        </td>

                                        <td class="col-md-1 text-center">
                                            <strong>৳{{ $item->price }}

                                            </strong>

                                        <td class="col-md-1 text-center">
                                            <a href="#">write
                                                a review</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
