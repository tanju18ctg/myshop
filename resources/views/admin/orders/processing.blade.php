@extends('layouts.admin-master')

@section('orders')
    active show-sub
@endsection

@section('processing-orders')
    active
@endsection
@section('admin-content')
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a href="#" class="breadcrumb-item">
                Shop Mama </a>
            <span class="breadcrumb-item active"> processing Orders </span>
        </nav>
        <div class="sl-pagebody">
            <div class="row row-sm">
                <!--------list-page------->
                <div class="col-md-12">
                    <div class="card pd-20 pd-sm-40">
                        <h6 class="card-body-title">Basic Responsive DataTable</h6>
                        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to
                            the table, as shown in this example.</p>

                        <div class="table-wrapper">
                            <table id="datatable1" class="table display responsive nowrap">
                                <thead>
                                    <tr>
                                        <th class="wd-25p"> Date</th>
                                        <th class="wd-25p">Invoice </th>
                                        <th class="wd-25p">Amount</th>
                                        <th class="wd-25p">Tnx Id</th>
                                        <th class="wd-25p">Status</th>
                                        <th class="wd-25p">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $order)
                                        <tr>
                                            <td> {{ $order->order_date }} </td>
                                            <td> {{ $order->invoice_no }} </td>
                                            <td> {{ $order->amount }} </td>
                                            <td> {{ $order->transaction_id }} </td>
                                            <td> {{ $order->status }} </td>
                                            <td>
                                                <a href="{{ route('order-view', $order->id) }}"
                                                    class="btn btn-primary btn-sm"><i class="fa fa-eye"> </i></a>
                                                    <a href="{{ url('admin/generate-pdf/'.$order->id) }}" class="btn btn-sm btn-danger "><i
                                                        class="fa fa-download" style="color:white;"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-wrapper -->
                    </div><!-- card -->
                </div>
            </div>

        </div>
    </div>
@endsection
