<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        if(Auth::check() && Auth::user()->isban){
            $banned = Auth::user()->isban == '1';
            Auth::logout();
            if($banned == 1){
                $message = 'You account has been banned please contact with Admin';
            }

            return Redirect()->route('login')->with('status', $message)->withErrors([
                'banned' => 'You account has been banned. please contact with administrator'
            ]);
        }

        // user online & offline

        if(Auth::check()){
            $expiredTime =  Carbon::now()->addSeconds(30);
            Cache::put('user-isOnline' .Auth::user()->id, true, $expiredTime);
  
            User::where('id', Auth::user()->id)->update(['last_seen' => Carbon::now()]);
  
          }



        if (Auth::check() && Auth::user()->role_id == 2) {
            return $next($request);
        } else {
            return redirect()->route('login');
        }

       
    }
}
