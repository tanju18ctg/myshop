<?php

namespace App\Http\Middleware;

use Route;
use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    use \App\traits\AdminPermission;
    public function handle(Request $request, Closure $next)
    {
        if($this->checkRequestPermission()){
            return Response()->view('admin.home');
        }
        return $next($request);
       
    }
}
