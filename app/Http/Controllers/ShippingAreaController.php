<?php

namespace App\Http\Controllers;

use App\Models\ShippingDistrict;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ShippingDivision;
use App\Models\ShippingState;

class ShippingAreaController extends Controller
{
    //division create

    public function create(){
        $division = ShippingDivision::orderBy('id', 'DESC')->get();
        return view('admin.shippingDivision.create', compact('division'));
    }

    // division store

    public function divisionStore(Request $request){
        $request->validate([
            'division_name' => 'required : unique',
        ]);

        ShippingDivision::insert([
            'division_name' => $request->division_name,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Division Store Success',
            'alert-type' => 'success'
        );
         return Redirect()->back()->with($notification);
    }

    //division edit

    public function divisionEdit($id) {
        $division = ShippingDivision::findOrFail($id);

        return view('admin.shippingDivision.edit', compact('division'));
    }

    //division update

    public function divisionUpdate(Request $request){
        $id = $request->id;

        $request->validate([
            'division_name' => 'required : unique',
        ]);


        ShippingDivision::findOrFail($id)->update([
            'division_name' => $request->division_name,
            'updated_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Division Update Success',
            'alert-type' => 'success'
        );
         return Redirect()->route('add-division')->with($notification);
    }

    // Division Destroy

    public function divisionDestroy($id) {
        ShippingDivision::findOrFail($id)->delete();
        $notification=array(
            'message' => 'Division Delete Success',
            'alert-type' => 'success'
        );
         return Redirect()->back()->with($notification);
    }


    // district create
    public function districtCreate() 
    {
        $districts = ShippingDistrict::with('division')->orderBy('id', 'DESC')->get();
        $divisions = ShippingDivision::orderBy('division_name', 'ASC')->get();
        return view('admin.shippingDistrict.create', compact('districts','divisions'));
    }

    // district store 

    public function districtStore(Request $request)
    {
        $request->validate([
            'division_id' => 'required : unique',
            'district_name' => 'required : unique',
        ], [
            'division_id.required' => 'Please Select your division'
        ]);

        ShippingDistrict::insert([
            'division_id' => $request->division_id,
            'district_name' => $request->district_name,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'District Store Success',
            'alert-type' => 'success'
        );
         return Redirect()->route('add-district')->with($notification);
    }

    //district edit

    public function districtEdit($id) {
        $district = ShippingDistrict::findOrFail($id);
        $divisions = ShippingDivision::orderBy('division_name', 'ASC')->get();
        return view('admin.shippingDistrict.edit', compact('district', 'divisions'));

    }

    //district update

    public function districtUpdate(Request $request){
            $id = $request->id;

            $request->validate([
                'division_id' => 'required : unique',
                'district_name' => 'required : unique',
            ], [
                'division_id.required' => 'Please Select your division'
            ]);

            ShippingDistrict::findOrFail($id)->update([
                'division_id' => $request->division_id,
                'district_name' => $request->district_name,
                'updated_at' => Carbon::now()
            ]);

            $notification=array(
                'message' => 'District Update Success',
                'alert-type' => 'success'
            );
             return Redirect()->route('add-district')->with($notification);
    }

    // district delete

    public function districtDestroy($id){
        ShippingDistrict::findOrFail($id)->delete();
        $notification=array(
            'message' => 'District Delete Success',
            'alert-type' => 'success'
        );
         return Redirect()->back()->with($notification);
    }


    //=============================state methodes======================//
    public function stateCreate(){
        $districts = ShippingDistrict::orderBy('id', 'DESC')->get();
        $divisions = ShippingDivision::orderBy('division_name', 'ASC')->get();
        $states = ShippingState::with('division', 'district')->orderBy('id', 'DESC')->get();
        return view('admin.shippingState.create', compact('divisions', 'districts', 'states'));
    }

    public function getDistrictAjax($id) 
    {
        $district =  ShippingDistrict::where('division_id', $id)->get();
        return json_encode($district);
    }

    //state store 

    public function stateStore(Request $request){
        $request->validate([
            'division_id' => 'required : unique',
            'district_id' => 'required : unique',
            'state_name' => 'required : unique',
        ], [
            'division_id.required' => 'Please Select your division',
            'district_id.required' => 'Please Select your District',
            'state_name.required' => 'Please Select your State',
        ]);

        ShippingState::insert([
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'state_name' => $request->state_name,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'State Store Success',
            'alert-type' => 'success'
        );
         return Redirect()->route('add-state')->with($notification);
    }

    //state Edit

    public function stateEdit($id){
        $state = ShippingState::findOrFail($id);
        $divisions = ShippingDivision::orderBy('division_name', 'ASC')->get();

        return view('admin.shippingState.edit', compact('state', 'divisions'));
    }


    // state update

    public function stateUpdate(Request $request) {
       $id = $request->id;
        $request->validate([
            'division_id' => 'required : unique',
            'state_name' => 'required : unique',
        ], [
            'division_id.required' => 'Please Select your division'
        ]);

        ShippingState::findOrFail($id)->update([
            'division_id' => $request->division_id,
            'state_name' => $request->state_name,
            'updated_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'District Update Success',
            'alert-type' => 'success'
        );
         return Redirect()->route('add-state')->with($notification);
    }

    //state delete

    public function stateDestroy($id){
        ShippingState::findOrFail($id)->delete();
        $notification=array(
            'message' => 'Satate Delete Success',
            'alert-type' => 'success'
        );
         return Redirect()->back()->with($notification);
    }
}
