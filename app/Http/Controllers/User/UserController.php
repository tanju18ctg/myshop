<?php

namespace App\Http\Controllers\User;

use PDF;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller

{
    public function index()
    {
        return view('user.home');
    }

    // update user profile //

    public function updateData(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required'
            ],
            [
                'name.required' => 'please input your name',
                'phone.required' => 'please input your phone',
                'email.required' => 'please input your email',
            ]
        );

        User::findOrFail(Auth::id())->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
        ]);

        $notification = array(
            'message' => 'User Profile Updated',
            'alert-type' => 'success'
        );

        return Redirect()->back()->with($notification);
    }

    //-------------------user-image-------------------// 

    public function userImage()
    {
        return view('user.image-change');
    }

    //-------------------user-image-update------------//

    public function imageUpdate(Request $request)
    {

        $old_image =   $request->old_image;

        if (Auth::user()->image === 'backend/media/forWeb.jpg') {
            $image = $request->file('image');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save('backend/media/' . $name_gen);
            $save_url = 'backend/media/' . $name_gen;
            User::findOrFail(Auth::id())->update([
                'image' => $save_url
            ]);

            $notification = array(
                'message' => 'Your Image Updated',
                'alert-type' => 'success'
            );

            return Redirect()->back()->with($notification);
        } else {
            unlink($old_image);
            $image = $request->file('image');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(120, 120)->save('backend/media/' . $name_gen);
            $save_url = 'backend/media/' . $name_gen;
            User::findOrFail(Auth::id())->update([
                'image' => $save_url
            ]);

            $notification = array(
                'message' => 'Your Image Updated',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        }
    }

    //-----------------Update Password Change--------------------// 

    public function updatePassChange()
    {
        return view('user.password-change');
    }

    public function passwordStore(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8',
        ]);

        $db_password = Auth::user()->password;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;

        if (Hash::check($old_password, $db_password)) {
            if ($new_password === $confirm_password) {
                User::findOrFail(Auth::id())->update([
                    'password' => Hash::make($new_password)
                ]);

                Auth::logout();

                $notification = array(
                    'message' => 'Password Changed Successfuly',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            } else {
                $notification = array(
                    'message' => 'New Password & confirm password does not match',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            }
        } else {
            $notification = array(
                'message' => 'Old password not correct',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        }
    }


    //user order
    public function orderCreate()
    {

        $orders = Order::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
        return view('user.order.orderCreate', compact('orders'));
    }

    //order view

    public function orderView($order_id)
    {
        $order = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->where('user_id', Auth::id())->first();
        $orderItem = OrderItem::with('product')->where('order_id', $order_id)->get();
        return view('user.order.orderView', compact('order', 'orderItem'));
    }

    // generate pdf invoice

   public function generatePDF($order_id){

    $order = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->where('user_id', Auth::id())->first();
    $orderItem = OrderItem::with('product')->where('order_id', $order_id)->get();

    $pdf = PDF::loadView('user.order.voucher', compact('order','orderItem'));
    return $pdf->download('vouchers.pdf');
   }


   //user return request Submit

   public function returnSubmit(Request $request)
   {
        $id = $request->id;
        Order::findOrFail($id)->update([
            'return_date' => Carbon::now()->format('d F Y'),
            'return_reason' => $request->return_reason
        ]);

        $notification = array(
            'message' => 'Return Request Send Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('user-order')->with($notification);
   }


   // user Return order view 
   public function returnOrderView()
   {
        $orders = Order::where('return_reason', '!=', NULL)->get();
        return view('user.order.returnOrders', compact('orders'));
   }

   // user Cancel Order

   public function cancelOrder(){
        $orders = Order::where('status', 'cancel')->get();
        return view('user.order.cancel', compact('orders'));
   }
}
