<?php

namespace App\Http\Controllers\User;

use Carbon\Carbon;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    //review create

    public function reviewCreate($product_id){
        $id = $product_id;
        return view('user.order.createReview', compact('id')); 
    }


    //store review 

    public function reviewStore(Request $request){

        $product_id = $request->id;
        $request->validate([
            'rating' => 'required',
            'comment' => 'required',
        ]);

        Review::insert([
            'user_id' => Auth::id(),
            'product_id' => $product_id,
            'rating' => $request->rating,
            'comment' => $request->comment,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Product Review Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('user-order')->with($notification);
    }
}
