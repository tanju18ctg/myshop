<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Models\ShippingState;
use App\Models\ShippingDistrict;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    //get district

    public function districtWithAjax($id)
    {
        $district =  ShippingDistrict::where('division_id', $id)->get();
        return json_encode($district);
    }

    //get state
    public function stateWithAjax($id)
    {
        $state = ShippingState::where('district_id', $id)->get();
        return json_encode($state);
    }

    //check out Store

    public function checkoutStore(Request $request)
    {
        $data = array();

        $data['shipping_name'] = $request->shipping_name;
        $data['shipping_email'] = $request->shipping_email;
        $data['shipping_phone'] = $request->shipping_phone;
        $data['post_code'] = $request->post_code;
        $data['division_id'] = $request->division_id;
        $data['district_id'] = $request->district_id;
        $data['state_id'] = $request->state_id;
        $data['notes'] = $request->notes;
        $data['payment_method'] = $request->payment_method;
        $total = Cart::total();
        $carts = Cart::content();

        if (Session::has('coupon')) {
            $total_amount = Session::get('coupon')['total_amount'];
        } else {
            $total_amount = Cart::total();
            $a = $total_amount;
            $total_amount = str_replace(',', '', $a);
        }

        if ($request->payment_method == 'stripe') {
            return view('frontend.payment.stripe', compact('data', 'total'));
        } elseif ($request->payment_method == 'sslHost') {
            return view('frontend.payment.hostPayment', compact('data', 'total', 'carts', 'total_amount'));
        } elseif ($request->payment_method == 'sslEasy') {
            return view('frontend.payment.easyPayment', compact('data', 'total', 'carts', 'total_amount'));
        } else {
            return 'hand cash';
        }
    }
}
