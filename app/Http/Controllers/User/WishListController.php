<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WishList;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    //create
    public function create(){
        return view('user.wishList');
    }


    //Read Wish List
    public function getWishList(){
      $wishList =  WishList::with('product')->where('user_id', Auth::id())->latest()->get();
      return response()->json($wishList);
    }


    // Wish List Remove

    public function wishListRemove($id){

      WishList::where('user_id', Auth::id())->where('id', $id)->delete();
      return response()->json(['success', 'Item removed']);

    }
  
}
