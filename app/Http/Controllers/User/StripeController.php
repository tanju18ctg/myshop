<?php

namespace App\Http\Controllers\user;

use Carbon\Carbon;
use App\Models\Order;
use App\Mail\OrderMail;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class StripeController extends Controller
{
    public function store(Request $request)
    {
        \Stripe\Stripe::setApiKey('sk_test_fAbqMVCkCwIqxFL7nhfgpG1e00RazfME62');
        $token = $_POST['stripeToken'];

        if(Session::has('coupon')){
            $total_amount = Session::get('coupon')['total_amount'];
        }else{
            $total_amount = Cart::total();
        }

        $charge = \Stripe\Charge::create([
            'amount' => 999*100,
            'currency' => 'usd',
            'description' => 'Payment from Tarekul',
            'source' => $token,
            'metadata' => ['order_id' => uniqid()],
        ]);

        
        $order_id = Order::insertGetId([
                'user_id' => 123,
                'division_id' => $request->division_id,
                'district_id' => $request->district_id,
                'state_id' => $request->state_id,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'post_code' => $request->post_code,
                'notes' => $request->notes,
                'post_code' => $request->post_code,
                'payment_type' => 'stripe',
                'payment_method' => 'stripe',
                'transaction_id' => $charge->balance_transaction,
                'currency' => $charge->currency,
                'amount' => $total_amount,
                'order_number' => $charge->metadata->order_id,
                'invoice_no' => 'spm'.mt_rand(10000000, 900000000),
                'order_date' => Carbon::now()->format('d F Y'),
                'order_month' => Carbon::now()->format('F'),
                'order_year' => Carbon::now()->format('Y'),
                'status' => 'pending',
                'created_at' => Carbon::now(),
        ]);

        //stripe mail
       $orderData =  Order::findOrFail($order_id);

        $data = [
            'invoice_no' => $orderData->invoice_no,
            'amount' => $total_amount
        ];

        Mail::to($request->email)->send(new OrderMail($data));

        $carts  = Cart::content();

        foreach($carts as $cart){

            OrderItem::insert([
                'order_id' =>  $order_id,
                'product_id' => $cart->id,
                'qty' => $cart->qty,
                'price' => $cart->price,
                'size' => $cart->options->size,
                'color' => $cart->options->color
            ]);

        }

        foreach($carts as $cart)
        {
            Product::where('id', $cart->id)->decrement('product_qty', $cart->qty);
        }

        if(Session::has('coupon')){
            Session::forget('coupon');
        }

        Cart::destroy();

        $notification = array(
            'message' => 'Your Order Place Successful',
            'alert-type' => 'success'
        );
        
        return Redirect()->route('user.dashboard')->with($notification);
    }
}
