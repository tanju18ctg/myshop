<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected function redirectTo(){
        if (Auth()->user()->role_id ==1 ) {
            return route('admin.dashboard');

        }elseif (Auth()->user()->role_id ==2 ) {
           return route('user.dashboard');
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //socialite login

    public function googleRedirect(){
        return Socialite::driver('google')->stateless()->redirect();
       
    }

    public function googleCallback(){
        $user = Socialite::driver('google')->stateless()->user(); 
        $this->registerOrLogin($user);
        return redirect()->route('user.dashboard');
        
    }

    public function facebookRedirect(){
        return Socialite::driver('facebook')->stateless()->redirect();
    }
    
    public function facebookCallback(){
        $user = Socialite::driver('facebook')->stateless()->user(); 
        $this->registerOrLogin($user);
        return redirect()->route('user.dashboard');
    }

    public function registerOrLogin($data){
        $user = User::where('email', '=', $data->email)->first();
        if(!$user){
            $user = new User();
            $user->name = $data->name;
            $user->email = $data->email;
            $user->provider_id = $data->id;
            $user->image = $data->avatar;
            $user->save();
        }


        Auth::login($user);
    }

}
