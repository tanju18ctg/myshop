<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Brand;
use App\Models\Review;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Category;
use App\Models\MultiImage;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends Controller
{
    // index function 
    public function index()
    {

        $specialDeals = Product::where('special_deals', 1)->where('status', 1)->orderBy('id', 'DESC')->get();
        $specialProduct = Product::where('special_offer', 1)->where('status', 1)->orderBy('id', 'DESC')->get();
        $futureProduct = Product::where('futured', 1)->where('status', 1)->orderBy('id', 'DESC')->get();
        $products = Product::where('status', 1)->orderBy('id', 'DESC')->get();
        $sliders = Slider::where('status', 1)->orderBy('id', 'DESC')->limit(5)->get();
        $skip_categories_0 = Category::skip(0)->first();
        $skip_categories_1 = Category::skip(2)->first();
        $skip_brands_1 = Brand::skip(0)->first();
        $skip_category_products_0 = Product::where('status', 1)->where('category_id', $skip_categories_0->id)->orderBy('id', 'DESC')->get();
        $skip_category_products_1 = Product::where('status', 1)->where('category_id', $skip_categories_1->id)->orderBy('id', 'DESC')->get();
        $skip_brand_products_1 = Product::where('status', 1)->where('brand_id', $skip_brands_1->id)->orderBy('id', 'DESC')->get();

        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('frontend.index', compact('categories', 'sliders', 'products', 'futureProduct', 'specialProduct', 'specialDeals', 'skip_categories_0', 'skip_category_products_0', 'skip_categories_1', 'skip_category_products_1', 'skip_brands_1', 'skip_brand_products_1'));
    }

    //single product details

    public function singleProduct($product_id, $slug)
    {
        $product = Product::findOrFail($product_id);

        //prodoct color
        $color_en = $product->product_color_en;
        $product_color_en = explode(',', $color_en);

        $color_bn = $product->product_color_bn;
        $product_color_bn = explode(',', $color_bn);

        //product-size
        $size_en = $product->product_size_en;
        $product_size_en = explode(',', $size_en);

        $size_bn = $product->product_size_bn;
        $product_size_bn = explode(',', $size_bn);

        $multiImages = MultiImage::where('product_id', $product_id)->get();
        
        $cat_id = $product->category_id;

        $relatedProducts = Product::where('category_id', $cat_id)->where('id', '!=', $product->id)->orderBy('id', 'DESC')->get();

        $product_reviews = Review::where('product_id', $product_id)->where('status', 'approve')->latest()->get();
        $review_avrg = Review::where('product_id', $product_id)->where('status', 'approve')->latest()->avg('rating');
        $avrg_rating = number_format($review_avrg,1);
        return view("frontend.singleProduct", compact('product', 'multiImages', 'product_color_en', 'product_color_bn', 'product_size_en', 'product_size_bn', 'relatedProducts', 'product_reviews', 'avrg_rating'));
    }

    //-product tag wise

    public function tagWiseProduct($tag)
    {
        $product_tags = Product::where('status', 1)->where('product_tags_en', $tag)->orWhere('product_tags_bn', $tag)->orderBy('id', 'DESC')->paginate(1);
        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('Frontend.product-tag', compact('categories', 'product_tags'));
    }



    //-subcateWise Product 

    public function subcatWiseProduct(Request $request, $subCatId, $slug)
    {
        
        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        $route = 'subcategory/product';
        $subCatId = $subCatId;
        $subCatSlug = $slug;
        $sort = '';
        if($request->sort != null){
            $sort= $request->sort;
        }
       
        if($subCatId == null){
            return view('errors.404');
        }else{
            if($sort == 'priceLowToHigh'){
                $product_tags = Product::where(['status' => 1, 'subcategory_id' => $subCatId])->orderBy('selling_price', 'ASC')->paginate(12);
            }elseif($sort == 'priceHighToLow'){
                $product_tags = Product::where(['status' => 1, 'subcategory_id' => $subCatId])->orderBy('selling_price', 'DESC')->paginate(12);
            }elseif($sort == 'nameAtoZ'){
                $product_tags = Product::where(['status' => 1, 'subcategory_id' => $subCatId])->orderBy('product_name_en', 'ASC')->paginate(12);
            }elseif($sort == 'nameZtoA'){
                $product_tags = Product::where(['status' => 1, 'subcategory_id' => $subCatId])->orderBy('product_name_en', 'DESC')->paginate(12);
            }else{
                $product_tags = Product::where('status', 1)->where('subcategory_id', $subCatId)->orderBy('id', 'DESC')->paginate(3);
            }
        }

        //loadmore products with ajax

        // if($request->ajax())
        // {
        //     $grid_view = view('frontend.inc.grid-view-product', compact('product_tags'))->render();
        //     $list_view = view('frontend.inc.list-view-product', compact('product_tags'))->render();
        //     return response()->json(['grid_view' => $grid_view, 'list_view' => $list_view]);

        // }

        if ($request->ajax()) {
            $grid_view = view('frontend.inc.grid-view-product',compact('product_tags'))->render();
            $list_view = view('frontend.inc.list-view-product',compact('product_tags'))->render();
            return response()->json(['grid_view' => $grid_view,'list_view'=>$list_view]);
        }


        return view('frontend.subcategory-product', compact('product_tags', 'categories', 'route', 'subCatId', 'subCatSlug', 'sort'));
    }

    //-subsubcatWise Product Show 

    public function subsubcatWiseProduct($subsubCatId, $slug)
    {
        $product_tags = Product::where('status', 1)->where('subsubcategory_id', $subsubCatId)->orderBy('id', 'DESC')->paginate(1);
        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('frontend.subsubcategory-product', compact('product_tags', 'categories'));
    }




    //Product view modal with ajax 

    public function productView($product_id)
    {
        $product = Product::with('category', 'brand')->findOrFail($product_id);

        $color_en =  $product->product_color_en;
        $product_color_en = explode(',', $color_en);

        $size_en =  $product->product_size_en;
        $product_size_en = explode(',', $size_en);

        return response()->json(array(
            'product' => $product,
            'color' => $product_color_en,
            'size' => $product_size_en,
        ));
    }
}
