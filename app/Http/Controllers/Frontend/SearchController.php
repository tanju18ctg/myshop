<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Slider;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    //product search

    public function productSearch(Request $request){
       $product_tags = Product::where('product_name_en', 'LIKE','%'.$request->search.'%')
                            ->orWhere('product_name_bn', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('product_tags_en', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('product_tags_bn', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('short_description_en', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('short_description_bn', 'LIKE', '%'.$request->search.'%')->paginate();

                return view('frontend.search-product.index', compact('product_tags'));
    }


    // find Product

    public function findProduct(Request $request) {
        $products = Product::where('product_name_en', 'LIKE','%'.$request->search.'%')
                            ->orWhere('product_name_bn', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('product_tags_en', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('product_tags_bn', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('short_description_en', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('short_description_bn', 'LIKE', '%'.$request->search.'%')->take(5)->get();

                return view('frontend.search-product.findProduct', compact('products'));
    }


    public function index(){
       $sliders = Slider::latest()->get();
        return view('frontend.home', compact('sliders'));
    }
}
