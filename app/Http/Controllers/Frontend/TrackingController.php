<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackingController extends Controller
{
    //order tracking

    public function orderTracking(Request $request)
    {
        $order = Order::with('division', 'district', 'state', 'user')->where('invoice_no', $request->invoice_no)->first();
        if($order){
            $orderItems = OrderItem::with('product')->where('order_id', $order->id)->get();
        }else{
            $notification=array(
                'message' => 'Order Not Found',
                'alert-type' => 'error'
            );
              return Redirect()->back()->with($notification);
        }
        
        return view('frontend.order-tracking.index', compact('order', 'orderItems'));
    }
}
