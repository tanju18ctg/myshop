<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    //for language 

    public function english(){
        session()->get('language');
        session()->forget('language');
        Session::put('language', 'english');
        return Redirect()->back();
    }
    public function bangla(){
        session()->get('language');
        session()->forget('language');
        Session::put('language', 'bangla');
        return Redirect()->back();
    }
}
