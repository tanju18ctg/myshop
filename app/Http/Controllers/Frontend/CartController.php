<?php

namespace App\Http\Controllers\frontend;

use Carbon\Carbon;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\WishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShippingDivision;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;


class CartController extends Controller
{
    //add to card

    public function addToCart(Request $request, $id)
    {

        $product = Product::findOrFail($id);

        if ($product->discount_price == null) {
            Cart::add([
                'id' => $id,
                'name' => $request->product_name,
                'qty' => $request->quantity,
                'price' => $product->selling_price,
                'weight' => 1,
                'options' => [
                    'size' => $request->size,
                    'color' => $request->color,
                    'image' => $product->product_thumbnail
                ]
            ]);
            return response()->json(['success' => 'Cart successfully added']);
        } else {
            Cart::add([
                'id' => $id,
                'name' => $request->product_name,
                'qty' => $request->quantity,
                'price' => $product->discount_price,
                'weight' => 1,
                'options' => [
                    'size' => $request->size,
                    'color' => $request->color,
                    'image' => $product->product_thumbnail
                ]
            ]);
            return response()->json(['success' => 'Cart successfully added']);
        }
    }


    //product mini cart 

    public function miniCart()
    {

        $carts = Cart::content();
        $cartQty = Cart::count();
        $cartTotal = Cart::total();
        return response()->json(array(
            'carts' => $carts,
            'cartQty' => $cartQty,
            'cartTotal' => $cartTotal
        ));
    }


    //mini-cart-remove

    public function miniCartRemove($rowId)
    {
        Cart::remove($rowId);
        return response()->json(['success', 'Item removed']);
    }


    //Add to Wish List 
    public function addToWish(Request $request, $product_id)
    {

        if (Auth::check()) {

            $exixtsProduct = WishList::where('user_id', Auth::id())->where('product_id', $product_id)->first();

            if (!$exixtsProduct) {
                WishList::insert([
                    'user_id' => Auth::id(),
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()
                ]);
                return response()->json(['success', 'Add to wishList successfull']);
            } else {
                return response()->json(['error', 'This product already selected in the wish list']);
            }
        } else {
            return response()->json(['error', 'Login First your account']);
        }
    }



    // cart functionality  

    // cart page create 
    public function create()
    {
        return view('user.cart-page');
    }

    // get cart product

    public function getcart()
    {
        $carts = Cart::content();
        $quantity = Cart::count();
        $total = Cart::total();

        return response()->json(array(
            'carts' => $carts,
            'quantity' => $quantity,
            'total' => $total,
        ));
    }


    // remove cart page
    public function destroy($rowId)
    {
        if (Session::has('coupon')) {
            Session::forget('coupon');
        }
        Cart::remove($rowId);
        return response()->json(['success', 'Item removed']);
    }


    // cart product quantity increment
    public function increment($rowId)
    {

        if (Session::has('coupon')) {
            $coupon_name = Session::get('coupon')['coupon_name'];
            $coupon = Coupon::where('coupon_name', $coupon_name)->first();
            // cart total calculation 
            $cartTotal = Cart::total();
            $cartTotal = intval(preg_replace('/[^\d. ]/', '', $cartTotal));

            Session::put('coupon', [
                'coupon_name' => $coupon->coupon_name,
                'coupon_discount' => $coupon->coupon_discount,
                'discount_amount' => $cartTotal * $coupon->coupon_discount / 100,
                'total_amount' => $cartTotal - $cartTotal * $coupon->coupon_discount / 100
            ]);
        }
        $row = Cart::get($rowId);
        Cart::update($rowId, $row->qty + 1);
        if (Session::has('coupon')) {
            $coupon_name = Session::get('coupon')['coupon_name'];
            $coupon = Coupon::where('coupon_name', $coupon_name)->first();
            // cart total calculation 
            $cartTotal = Cart::total();
            $cartTotal = intval(preg_replace('/[^\d. ]/', '', $cartTotal));
            Session::put('coupon', [
                'coupon_name' => $coupon->coupon_name,
                'coupon_discount' => $coupon->coupon_discount,
                'discount_amount' => $cartTotal * $coupon->coupon_discount / 100,
                'total_amount' => $cartTotal - $cartTotal * $coupon->coupon_discount / 100
            ]);
        }
        return response()->json();
    }

    // cart product quantity decrement
    public function decrement($rowId)
    {


        $row = Cart::get($rowId);
        if ($row->qty == 1) {
            return response()->json('not decrement');
        } else {
            Cart::update($rowId, $row->qty - 1);
            if (Session::has('coupon')) {
                $coupon_name = Session::get('coupon')['coupon_name'];
                $coupon = Coupon::where('coupon_name', $coupon_name)->first();
                // cart total calculation 
                $cartTotal = Cart::total();
                $cartTotal = intval(preg_replace('/[^\d. ]/', '', $cartTotal));
                Session::put('coupon', [
                    'coupon_name' => $coupon->coupon_name,
                    'coupon_discount' => $coupon->coupon_discount,
                    'discount_amount' => $cartTotal * $coupon->coupon_discount / 100,
                    'total_amount' => $cartTotal - $cartTotal * $coupon->coupon_discount / 100
                ]);
            }
        }
        return response()->json('decrement');
    }


    // coupon apply

    public function couponApply(Request $request)
    {
        $coupon =  Coupon::where('coupon_name', $request->coupon_name)->where('coupon_validaty', '>=', Carbon::now()->format('Y-m-d'))->first();
        $cartTotal = Cart::total();
        $cartTotal = intval(preg_replace('/[^\d. ]/', '', $cartTotal));

        if ($coupon) {
            Session::put('coupon', [
                'coupon_name' => $request->coupon_name,
                'coupon_discount' => $coupon->coupon_discount,
                'discount_amount' => $cartTotal * $coupon->coupon_discount / 100,
                'total_amount' => $cartTotal - $cartTotal * $coupon->coupon_discount / 100
            ]);
            return response()->json(array(
                'validaty'=> true,
                'success'=> 'Coupon Apply successfull',
            ));
        } else {
            return response()->json(['error', 'Coupon Not Valid']);
        }
    }


    // coupon calculate

    public function couponCalculate()
    {
        if (Session::has('coupon')) {
            return response()->json(array(
                'sub_total' => Cart::total(),
                'coupon_name' => Session::get('coupon')['coupon_name'],
                'coupon_discount' => Session::get('coupon')['coupon_discount'],
                'discount_amount' => Session::get('coupon')['discount_amount'],
                'total_amount' => Session::get('coupon')['total_amount']
            ));
        } else {
            return response()->json(array(
                'total' => Cart::total(),
            ));
        }
    }


    // coupon remove
    public function couponRemove()
    {
        Session::forget('coupon');
        return response()->json();
        return response()->json(['success', 'Coupon Remove Successful']);
    }


    // create Checkout

    public function createCheckout()
    {
        if (Auth::check()) {
            if (Cart::total() > 0) {
                $carts = Cart::content();
                $quantity = Cart::count();
                $total = Cart::total();
                $divisions = ShippingDivision::orderBy('division_name', 'ASC')->get();
                return view('frontend.inc.checkout', compact('carts', 'quantity', 'total', 'divisions'));
            } else {
                $notification = array(
                    'message' => 'Shopping First',
                    'alert-type' => 'success'
                );
                return Redirect()->to('/')->with($notification);
            }
        } else {
            $notification = [
                'message' => 'Please Login First',
                'alert-type' => 'success'
            ];

            return redirect()->route('login')->with($notification);
        }
    }
}
