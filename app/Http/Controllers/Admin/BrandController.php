<?php

namespace App\Http\Controllers\Admin;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;

class BrandController extends Controller
{
    //---------------brand-index------------------//
    public function index(){
        $brands = Brand::latest()->get();
        return view('admin.brands.index', compact('brands'));
    }


    //------------------brand-store----------------//

    public function store(Request $request) 
    {
        $request->validate([
            'brand_name_en' => 'required',
            'brand_name_bn' => 'required',
            'brand_image' => 'required',
        ],[
            'brannd_name_en.required' => 'Please Enter your Brand Name in English',
            'brannd_name_bn.required' => 'Please Enter your Brand Name in Bangla',
            'brand_image.required' => 'Please Enter your Brand Name in Bangla',
        ]);



        $old_image = $request->old_image;
        $image =  $request->file('brand_image');
        $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(114, 110)->save('upload/brand/'.$image_path);
        $save_url = 'upload/brand/'.$image_path;
      

        Brand::insert([
            'brand_name_en' => $request->brand_name_en,
            'brand_name_bn' => $request->brand_name_bn,
            'brand_slug_en' => strtolower(str_replace( '', '_' , $request->brand_name_en)),
            'brand_slug_bn' => str_replace( '', '_' , $request->brand_name_bn),
            'brand_image' => $save_url,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Brand Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    //---------------brand-edit------------------//

    public function edit($id)
     {
       $brand = Brand::findOrFail($id);
       return view('admin/brands/edit', compact('brand'));

    }

    public function update(Request $request)
    {
        $brand_id = $request->id;
        $old_image = $request->old_image;
        $request->validate([
            'brand_name_en' => 'required',
            'brand_name_bn' => 'required',
        ],[
            'brannd_name_en.required' => 'Please Enter your Brand Name in English',
            'brannd_name_bn.required' => 'Please Enter your Brand Name in Bangla',
        ]);

    
        if($request->file('brand_image')){
            unlink($old_image);
            $image =  $request->file('brand_image');
            $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(114, 110)->save('upload/brand/'.$image_path);
            $save_url = 'upload/brand/'.$image_path;
      

        Brand::findOrFail($brand_id)->update([
            'brand_name_en' => $request->brand_name_en,
            'brand_name_bn' => $request->brand_name_bn,
            'brand_slug_en' => strtolower(str_replace( '', '_' , $request->brand_name_en)),
            'brand_slug_bn' => str_replace( '', '_' , $request->brand_name_bn),
            'brand_image' => $save_url,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Brand Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('brands')->with($notification);

        }else {

            Brand::findOrFail($brand_id)->update([
                'brand_name_en' => $request->brand_name_en,
                'brand_name_bn' => $request->brand_name_bn,
                'brand_slug_en' => strtolower(str_replace( '', '_' , $request->brand_name_en)),
                'brand_slug_bn' => strtolower(str_replace( '', '_' , $request->brand_name_bn)),
                'created_at' => Carbon::now()
            ]);
    
            $notification=array(
                'message' => 'Brand Store Success',
                'alert-type' => 'success'
            );
            return Redirect()->route('brands')->with($notification);
        }
    }
  
   

   //--------------delte-function -------------// 

   public function delete($id) {
       $brand = Brand::findOrFail($id);
      $img =  $brand->brand_image;
      unlink($img);
      Brand::findOrFail($id)->delete();

      $notification=array(
        'message' => 'Brand Delete Success',
        'alert-type' => 'success'
    );
      return Redirect()->back()->with($notification);
   }

}
