<?php

namespace App\Http\Controllers\Admin;
use PDO;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Models\SubSubCategory;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    ///------------store-function---------------//
    public function index() {
        $categories = Category::latest()->get();
        return view('admin.categories.index', compact('categories'));
    }

    public function store(Request $request) 
    {
        $request->validate([
            'category_name_en' => 'required',
            'category_name_bn' => 'required',

        ],[
            'category_name_en.required' => 'Please Enter your category Name in English',
            'category_name_bn.required' => 'Please Enter your category Name in Bangla',

        ]);

        Category::insert([
            'category_name_en' => $request->category_name_en,
            'category_name_bn' => $request->category_name_bn,
            'category_slug_en' => strtolower(str_replace( '', '_' , $request->category_name_en)),
            'category_slug_bn' => str_replace( '', '_' , $request->category_name_bn),
    
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Category Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function edit($id){
         $category = Category::findOrFail($id);
         return view('admin.categories.edit', compact('category'));
    }

    public function update(Request $request){
        $category_id = $request->id;

        Category::findOrFail($category_id)->update([
            'category_name_en' => $request->category_name_en,
            'category_name_bn' => $request->category_name_bn,
            'category_slug_en' => strtolower(str_replace( '', '_' , $request->category_name_en)),
            'category_slug_bn' => str_replace( '', '_' , $request->category_name_bn),
            'category_icon' => $request->category_icon,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Category Update Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('category')->with($notification);
    }

    public function delete($id){
        Category::findOrFail($id)->delete();
        $notification=array(
            'message' => 'Delete Category Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }


    //==============================Sub-category=========================//

    public function subCatindex(){
       $subCategories = Subcategory::latest()->get();
      $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('admin.sub-category.index', compact('subCategories', 'categories'));
    }

    public function Substore(Request $request){
   
            $request->validate([
            'subcategory_name_en' => 'required',
            'subcategory_name_bn' => 'required',
            'category_id' => 'required'
            ],
        [
            'subcategory_name_en.required' => 'please give subCategory Name En',
            'subcategory_name_bn.required' => 'please give subCategory Name Bn',
            'category_id.required' => 'please Choose Category Name',
        ]
        );
        
        Subcategory::insert([
            'subcategory_name_en' => $request->subcategory_name_en,
            'subcategory_name_bn' => $request->subcategory_name_bn,
            'category_id' => $request->category_id,
            'subcategory_slug_en' => strtolower(str_replace( '', '_' , $request->subcategory_name_en)),
            'subcategory_slug_bn' => str_replace( '', '_' , $request->subcategory_name_bn),
        ]);

        $notification=array(
            'message' => 'SubCategory Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }


    //----------subcategory delete
    public function subCatdelete($id)
    {
        Subcategory::findOrFail($id)->delete();
        $notification=array(
            'message' => 'SubCategory Delete Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    //--------------------sub-subcategory-controller---------------------//

   public function subSubCatindex()
    {
        $sub_subCategories = SubSubCategory::latest()->get();
      $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('admin.sub-subcategories.index', compact('sub_subCategories', 'categories'));
    }

    public function subSubCatStore(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'subsubCategory_name_en' => 'required',
            'subSubCategory_name_bn' => 'required',
            ],
        [
            'category_id.required' => 'please give Category Name En',
            'subcategory_id.required' => 'please give subCategory Name En',
            'subsubCategory_name_en.required' => 'please Give sub-subCategory Name En',
            'subSubCategory_name_bn.required' => 'please Give sub-subCategory Name Bn',
        ]
        );
        
        SubSubCategory::insert([
            'category_id' => $request->category_id,
            'subcategory_id' => $request->subcategory_id,
            'subsubCategory_name_en' => $request->subsubCategory_name_en,
            'subSubCategory_name_bn' => $request->subSubCategory_name_bn,
            'subSubCategory_slug_en' => strtolower(str_replace( '', '_' , $request->subsubCategory_name_en)),
            'subSubCategory_slug_bn' => str_replace( '', '_' , $request->subsubCategory_name_bn),
        ]);

        $notification=array(
            'message' => 'SubSubCategory Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }



    //-----public sub-cate-ajax-------------------// 
    public function getSubCat($cat_id)
    {

         $sub_cat = Subcategory::where('category_id', $cat_id)->orderBy('subcategory_name_en', 'ASC')->get();
        return json_encode($sub_cat);
    }

    //--------------------sub sub-cate-edit-------------------//

    public function subSubEdit($subSubid)
    {
       $subSubCat = SubSubCategory::find($subSubid);
       
       return view('admin.sub-subcategories.edit', compact('subSubCat'));
    }

    //---------update-method--------//

    public function subSubCatUpdate(Request $request)
    {
            $subSubCatId = $request->id; 
            SubSubCategory::findOrFail($subSubCatId)->update([
                'subsubCategory_name_en' => $request->subsubCategory_name_en,
                'subsubCategory_name_bn' => $request->subsubCategory_name_bn,
                'subSubCategory_slug_en' => strtolower(str_replace( '', '_' , $request->subsubCategory_name_en)),
                'subSubCategory_slug_bn' => str_replace( '', '_' , $request->subsubCategory_name_bn),
                'created_at' => Carbon::now()
            ]);
    
            $notification=array(
                'message' => 'sub sub Category Update Success',
                'alert-type' => 'success'
            );
            return Redirect()->route('sub-subcategory')->with($notification);

    }

    public function subSubDelete ($subSubCat)
    {
        SubSubCategory::findOrFail($subSubCat)->delete();
        $notification=array(
            'message' => 'sub sub Category Delete Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('sub-subcategory')->with($notification);
    }

}