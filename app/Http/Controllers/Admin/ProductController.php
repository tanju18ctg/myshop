<?php


namespace App\Http\Controllers\Admin;
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Models\MultiImage;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Models\SubSubCategory;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{

    //get data with ajax
    public function getSubSubCat($subcat_id) 
    {
        $subCate = SubSubCategory::where('subcategory_id', $subcat_id)->orderBy('subsubCategory_name_en','ASC')->get();
        return json_encode($subCate);
    }


    //product data showing index page 
    public function index()
    {
      $products = Product::latest()->get();
      return view('admin.product.index', compact('products'));
    }



    //add product method 
    public function addProduct()
    {
        $brands = Brand::latest()->get();
        $categories = Category::latest()->get();
        return view('admin.product.addProduct', compact('categories', 'brands'));
    }


    //Product store----//
    public function productStore(Request $request)
    {


        //Single Image Store
       $image = $request->file('thumbnail');
       $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
       Image::make($image)->resize(114, 110)->save('upload/product/thumb/'.$image_path);
       $save_url = 'upload/product/thumb/'.$image_path;
       $product_id =  Product::insertGetId([
        'brand_id' => $request->category_id,
        'category_id' => $request->category_id,
        'subcategory_id' => $request->subcategory_id,
        'subsubcategory_id' => $request->subsubcategory_id,
        'product_name_en' => $request->product_name_en,
        'product_name_bn' => $request->product_name_bn,
        'product_slug_en' => strtoupper(str_replace(',', '-', $request->product_name_en)) ,
        'product_slug_bn' => str_replace(',', '-', $request->product_name_bn),
        'product_code' => $request->product_code,
        'product_qty' => $request->product_qty,
        'product_tags_en' => $request->product_tags_en,
        'product_tags_bn' => $request->product_tags_bn,
        'product_size_en' => $request->product_size_en,
        'product_size_bn' => $request->product_size_bn,
        'product_color_en' => $request->product_color_en,
        'product_color_bn' => $request->product_color_bn,
        'selling_price' => $request->selling_price,
        'discount_price' => $request->discount_price,
        'short_description_en' => $request->short_description_en,
        'short_description_bn' => $request->short_description_bn,
        'long_description_en' => $request->long_description_en,
        'long_description_bn' => $request->long_description_bn,
        'hot_deals' => $request->hot_deals,
        'futured' => $request->futured,
        'special_offer' => $request->special_offer,
        'special_deals' => $request->special_deals,
        'product_thumbnail' => $save_url,
        'created_at' => Carbon::now(),
        
       ]);

       $notification=array(
        'message' => 'Product Store Success',
        'alert-type' => 'success'
    );


      //Multiple Image 
      $images =  $request->file('multiple');
      foreach($images as $img)
      {
        $image_path = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
        Image::make($img)->resize(970, 1100)->save('upload/product/multi-image/'.$image_path);
        $save_url = 'upload/product/multi-image/'.$image_path;
        MultiImage::insert([
          'product_id' => $product_id,
          'photo_name' => $save_url,
          'created_at' => Carbon::now()
        ]);
      }

      $notification=array(
        'message' => 'Multiple Image Store Success',
        'alert-type' => 'success'
    );

     return Redirect()->route('manage-product')->with($notification);

    }


    //edit product data 
    public function edit($id)
    {

      $brands = Brand::latest()->get();
      $categories = Category::latest()->get();
      $product =  Product::find($id);
      $multiImage = MultiImage::where('product_id', $id)->latest()->get();
      return view('admin.product.edit', compact('brands', 'categories', 'product', 'multiImage'));

    }

    //product update
    public function update(Request $request)
    {
        $product_id = $request->id;
      Product::findOrFail($product_id)->update([
        'brand_id' => $request->category_id,
        'category_id' => $request->category_id,
        'subcategory_id' => $request->subcategory_id,
        'subsubcategory_id' => $request->subsubcategory_id,
        'product_name_en' => $request->product_name_en,
        'product_name_bn' => $request->product_name_bn,
        'product_code' => $request->product_code,
        'product_qty' => $request->product_qty,
        'product_tags_en' => $request->product_tags_en,
        'product_tags_bn' => $request->product_tags_bn,
        'product_size_en' => $request->product_size_en,
        'product_size_bn' => $request->product_size_bn,
        'product_color_en' => $request->product_color_en,
        'product_color_bn' => $request->product_color_bn,
        'selling_price' => $request->selling_price,
        'discount_price' => $request->discount_price,
        'short_description_en' => $request->short_description_en,
        'short_description_bn' => $request->short_description_bn,
        'long_description_en' => $request->long_description_en,
        'long_description_bn' => $request->long_description_bn,
        'hot_deals' => $request->hot_deals,
        'futured' => $request->futured,
        'special_offer' => $request->special_offer,
        'special_deals' => $request->special_deals,
        'created_at' => Carbon::now(),
      ]);


      $notification=array(
        'message' => 'Product Update Success',
        'alert-type' => 'success'
      );
      return Redirect()->back()->with($notification);
    }



    //update product main thumb
    public function updateProductThumb(Request $request)
    {
      $product_id = $request->id;
      $old_image = $request->old_img;
      unlink($old_image);

      $image = $request->file('product_thumbnail');
      $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
      Image::make($image)->resize(114, 110)->save('upload/product/thumb/'.$image_path);
      $upload_url = 'upload/product/thumb/'.$image_path;

      Product::findOrFail($product_id)->update([
        'product_thumbnail' => $upload_url,
        'updated_at' => Carbon::now()
      ]);

      $notification=array(
        'message' => 'Product Thumbnail Update Success',
        'alert-type' => 'success'
      );
      return Redirect()->back()->with($notification);
    }





    //Update Product multiple image
    public function updateProductImage(Request $request)
    {
        $imgs = $request->multiImg;

        foreach ($imgs as $id => $img){
          $imgDelete = MultiImage::findOrFail($id);
          unlink($imgDelete->photo_name);

          $image_path = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
          Image::make($img)->resize(114, 110)->save('upload/product/multi-image/'.$image_path);
          $upload_url = 'upload/product/multi-image/'.$image_path;

          MultiImage::where('id', $id)->update([
            'photo_name' => $upload_url,
            'updated_at' => Carbon::now()
          ]);
        }
        $notification=array(
          'message' => 'Multiple Product Image Update Success',
          'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    //delete multiImage 

    public function deleteMultiImage($id)
    {
      $old_img = MultiImage::findOrFail($id);
      unlink($old_img->photo_name);
      MultiImage::findOrFail($id)->delete();

      $notification=array(
        'message' => 'Multiple Product Image Delete Success',
        'alert-type' => 'success'
      );
      return Redirect()->back()->with($notification);
    }



    //product inactive
      public function inactive($id)
      {
        Product::findOrFail($id)->update([
          'status' => 0
        ]);

        $notification=array(
          'message' => ' Product inactivated',
          'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
      }


  //product-active
      public function active($id)
      {
        Product::findOrFail($id)->update([
          'status' => 1
        ]);

        $notification=array(
          'message' => ' Product activated',
          'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
      }


}
