<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    public function index(){
        return view('admin.home');
    }

    //------------------Profile ------------//
    public function profile() {
        return view('admin.profile.index');
    }

    //--------------update data---------------//

    public function updateData(Request $request) {
       User::findOrFail(Auth::id())->update([
           'name' => $request->name,
           'phone' => $request->phone,
           'email' => $request->email
       ]);
       $notification = [
           'message' => 'Admin profile updated',
           'alert-type' => 'success'
       ];

       return redirect()->back()->with($notification);

    }

    public function adminImage() {
        return view("admin.profile.imageChange");
    }

    //-----------------Image-update----------------// 
    public function updateImage(Request $request){
       $old_image = $request->old_image;
       $image =  $request->file('image');
       $image_path = hexdec(uniqid()).$image->getClientOriginalExtension();
       Image::make($image)->resize(300, 300)->save('backend/media'.$image_path);
        $save_url = 'backend/media'.$image_path;
        User::find(Auth::id())->update([
            'image' => $save_url
        ]);

        $notification = array(
            'message' => 'Your Image Updated',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    // Admin password Change function //
    public function changePassword() {
        return view("admin.profile.changePassword");
    }


    public function adminPasswordStore(Request $request) {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8',
        ]);

        $db_password = Auth::user()->password;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;

        if(Hash::check($old_password,$db_password)) 
        {
            if($new_password === $confirm_password) {
                User::findOrFail(Auth::id())->update([
                    'password' => Hash::make($new_password)
                ]);

                Auth::logout();
                
                $notification = array(
                    'message' => 'Password Changed Successfuly',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);

            }else {
                $notification = array(
                    'message' => 'New Password & confirm password does not match',
                    'alert-type' => 'success'
                );
                return Redirect()->back()->with($notification);
            }

        }else{
            $notification = array(
                'message' => 'Old password not correct',
                'alert-type' => 'success'
            );
            return Redirect()->back()->with($notification);
        }
    }


    // Read all users

    public function readUsers(){
        $users = User::where('role_id', '!=', 1)->orderBy('id', 'DESC')->get();

        return view('admin.users.read-users', compact('users'));
    }


    // user banned & unbanned

    public function banned($user_id){
        User::findOrFail($user_id)->update(['isban' => 1]);

        $notification = array(
            'message' => 'User Banned Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('all-users')->with($notification);
    }

    
    public function unbanned($user_id){
        User::findOrFail($user_id)->update(['isban' => 0]);

        $notification = array(
            'message' => 'User Unbanned Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('all-users')->with($notification);
    }



}
