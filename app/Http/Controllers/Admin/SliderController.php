<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    //slider index
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('admin.sliders.index', compact('sliders'));
    }

    //slider store 

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required'
        ]);

        $image =  $request->file('image');
        $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(870, 370)->save('upload/slider/'.$image_path);
        $upload_url = 'upload/slider/'.$image_path;
        Slider::insert([
            'title_en' => $request->title_en,
            'title_bn' => $request->title_bn,
            'description_en' => $request->description_en,
            'description_bn' => $request->description_bn,
            'image' => $upload_url,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Slider Store Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    //slider edit 
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.edit', compact('slider'));
    }

    //slider update

    public function update(Request $request)
    {

            $id = $request->id;
            $old_image = $request->old_image;
        
        if($request->file('image')){
            unlink($old_image);
            $image =  $request->file('image');
            $image_path = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(870, 370)->save('upload/slider/'.$image_path);
            $upload_url = 'upload/slider/'.$image_path;
      

        Slider::findOrFail($id)->update([
            'title_en' => $request->title_en,
            'title_bn' => $request->title_bn,
            'description_en' => $request->description_en,
            'description_bn' => $request->description_bn,
            'image' => $upload_url,
            'updated_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Slider update Success',
            'alert-type' => 'success'
        );
        return Redirect()->route('sliders')->with($notification);

        }else {

            Slider::findOrFail($id)->update([
                'title_en' => $request->title_en,
                'title_bn' => $request->title_bn,
                'description_en' => $request->description_en,
                'description_bn' => $request->description_bn,
                'updated_at' => Carbon::now()
            ]);
    
            $notification=array(
                'message' => 'Slider Success',
                'alert-type' => 'success'
            );
            return Redirect()->route('sliders')->with($notification);
        }
    }

    // slider destroy 

    public function destroy($id)
    {
        $old_img =  Slider::findOrFail($id);
        unlink($old_img->image);
        Slider::findOrFail($id)->delete();

        $notification=array(
            'message' => 'Slider Delete Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    //inactive 
    public function inactive($id)
    {
        Slider::findOrFail($id)->update(['status' => 0]);
        $notification=array(
            'message' => 'Slider Inactive Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }


    //Slider Active 
    public function active($id)
    {
        Slider::findOrFail($id)->update(['status' => 1]);
        $notification=array(
            'message' => 'Slider Active Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }



}
