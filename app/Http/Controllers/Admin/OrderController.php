<?php

namespace App\Http\Controllers\Admin;

use PDF;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    //pending index
    public function pendingIndex(){
        $orders = Order::where('status', 'pending')->orderBy('id', 'DESC')->get();
        return view('admin.orders.pending', compact('orders'));
    }


    //order view
    public function orderView($order_id){
        $order = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->first();
        $orderItems = OrderItem::with('product')->where('order_id', $order_id)->get();
        return view('admin.orders.orderView', compact('order', 'orderItems'));
    }

    //order confirmed 
    public function confirmedIndex(){
        $orders = Order::where('status', 'confirmed')->orderBy('id', 'DESC')->get();
        return view('admin.orders.confirmed', compact('orders'));
    }

      //order confirmed 
      public function processingIndex(){
        $orders = Order::where('status', 'processing')->orderBy('id', 'DESC')->get();
        return view('admin.orders.processing', compact('orders'));
    }

     //order confirmed 
     public function pickedIndex(){
        $orders = Order::where('status', 'picked')->orderBy('id', 'DESC')->get();
        return view('admin.orders.picked', compact('orders'));
    }

     //order confirmed 
     public function shippedIndex(){
        $orders = Order::where('status', 'shipped')->orderBy('id', 'DESC')->get();
        return view('admin.orders.shipped', compact('orders'));
    }

     //order confirmed 
     public function deleveredIndex(){
        $orders = Order::where('status', 'delevered')->orderBy('id', 'DESC')->get();
        return view('admin.orders.delevered', compact('orders'));
    }

     //order confirmed 
     public function cancelIndex(){
        $orders = Order::where('status', 'cancel')->orderBy('id', 'DESC')->get();
        return view('admin.orders.cancel', compact('orders'));
    }

    //pedning to confirm

    public function pendingToConfirm($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'confirmed',
            'confirmed_date' => Carbon::now()->format('d F Y')
        ]);
        $notification=array(
            'message' => 'Confirmed Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('confirmed-orders')->with($notification);
    }

      //confirm to processing
    public function confirmToProcessing($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'processing',
            'processing_date' => Carbon::now()->format('d F Y'),
        ]);
        $notification=array(
            'message' => 'Processing Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('processing-orders')->with($notification);
    }

      //confirm to processing
    public function processingToPicked($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'picked',
            'picked_date' => Carbon::now()->format('d F Y')
        ]);
        $notification=array(
            'message' => 'Picked Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('picked-orders')->with($notification);
    }

      //confirm to processing
    public function pickedToShipped($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'shipped',
            'shipped_date' => Carbon::now()->format('d F Y')
        ]);
        $notification=array(
            'message' => 'Shipped Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('shipped-orders')->with($notification);
    }


      //confirm to processing
    public function shippedToDelevered($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'delevered',
            'delevered_date' => Carbon::now()->format('d F Y')
        ]);
        $notification=array(
            'message' => 'Delevered Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('delevered-orders')->with($notification);
    }


    //download invoice

    public function downloadInvoice($order_id){
        $order = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->first();
        $orderItem = OrderItem::with('product')->where('order_id', $order_id)->get();
    
        $pdf = PDF::loadView('admin.orders.voucher', compact('order','orderItem'));
        return $pdf->download('vouchers.pdf');
    }


    // cancel order 

    public function cancelOrder($order_id){
        Order::findOrFail($order_id)->update([
            'status' => 'cancel',
            'cancel_date' => Carbon::now()->format('d F Y')
        ]);
        $notification=array(
            'message' => 'Cancel Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('confirmed-orders')->with($notification);
    }



}
