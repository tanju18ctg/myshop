<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDO;

class StockController extends Controller
{
    //product stock index

    public function index(){
        $products= Product::latest()->get();

        return view('admin.stock.index', compact('products'));
    }

    // product stock edit

    public function edit($id){
        $product = Product::findOrFail($id);
        return view('admin.stock.edit', compact('product'));
    }

    //stock update

    public function update(Request $request, $product_id){
        Product::findOrFail($product_id)->update(['product_qty' => $request->product_qty]);

        $notification=array(
            'message' => 'Stock Update Success',
            'alert-type' => 'success'
        );
          return Redirect()->route('product-stock')->with($notification);
    }
}
