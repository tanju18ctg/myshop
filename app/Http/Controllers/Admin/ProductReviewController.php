<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductReviewController extends Controller
{
    //review create

    public function create()
    {

        $reviews = Review::latest()->get();
        return view('admin.review.create', compact('reviews'));
    }

    // review delete
    public function destroy($product_id)
    {
        Review::findOrFail($product_id)->delete();

        $notification = array(
            'message' => 'Product Review Delete Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }


    //review approve
    public function approve($review_id)
    {
        Review::findOrFail($review_id)->update([
            'status' => 'approve'
        ]);

        $notification = array(
            'message' => ' Review Approve Success',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);

    }


}
