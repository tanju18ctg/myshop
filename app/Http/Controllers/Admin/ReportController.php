<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    //report index

    public function index(){
        return view('admin.reports.index');
    }

    // search by date
    public function searchDate(Request $request)
    {
            $date =  $request->date;
            $formatDate = new DateTime($date);
            $formatedDate = $formatDate->format('d F Y');
            $orders = Order::where('order_date', $formatedDate)->latest()->get();
            return view('admin.reports.search-reports', compact('orders'));
    }

    //search by month

    public function searchMonth(Request $request){
        $orders = Order::where('order_month', $request->month)->where('order_year', $request->year)->latest()->get();

        return view('admin.reports.search-reports', compact('orders'));
    }

    //search by year

  
    public function searchYear(Request $request){
        $orders = Order::where('order_year', $request->year_name)->latest()->get();

        return view('admin.reports.search-reports', compact('orders'));
    }

}
