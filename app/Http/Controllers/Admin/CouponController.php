<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    //coupon create

    public function create() {
      $coupons =  Coupon::orderBy('id', 'DESC')->latest()->get();
        return view('admin.coupon.create', compact('coupons'));
    }

    //coupon store

    public function couponStore(Request $request){
        $request->validate([
            'coupon_name' => 'required : unique',
            'coupon_discount' => 'required : unique',
            'coupon_validaty' => 'required : unique'
        ]);


        Coupon::insert([
            'coupon_name' => $request->coupon_name,
            'coupon_discount' => $request->coupon_discount,
            'coupon_validaty' => $request->coupon_validaty,
            'status' => $request->status,
            'created_at' => Carbon::now()
        ]);

        $notification=array(
            'message' => 'Coupon Store Success',
            'alert-type' => 'success'
        );
    
         return Redirect()->back()->with($notification);
    }

    // coupon edit

    public function edit($id){
        $coupon = Coupon::findOrFail($id);
        return view('admin.coupon.edit', compact('coupon'));
    }

    public function update (Request $request){
       $coupon_id =  $request->id;
       $request->validate([
        'coupon_name' => 'required : unique',
        'coupon_discount' => 'required : unique',
        'coupon_validaty' => 'required : unique',
        'updated_at' => Carbon::now()
    ]);


    Coupon::findOrFail($coupon_id)->update([
        'coupon_name' => $request->coupon_name,
        'coupon_discount' => $request->coupon_discount,
        'coupon_validaty' => $request->coupon_validaty,
    ]);

    $notification=array(
        'message' => 'Coupon Update Success',
        'alert-type' => 'success'
    );

     return Redirect()->route('coupon')->with($notification);


    }
}
