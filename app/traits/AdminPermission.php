<?php 

namespace App\traits;

use Route;
use Illuminate\Support\Facades\Auth;
Trait AdminPermission {
    public function checkRequestPermission(){
        if(
            empty(Auth::user()->role->permission['permission']['slider']['list']) && \Route::is('sliders') ||
            empty(Auth::user()->role->permission['permission']['slider']['add']) && \Route::is('sliders') ||
            empty(Auth::user()->role->permission['permission']['product']['add']) && \Route::is('add-product') ||
            empty(Auth::user()->role->permission['permission']['product']['list']) && \Route::is('manage-product')||
            empty(Auth::user()->role->permission['permission']['brand']['list']) && \Route::is('brands')||
             empty(Auth::user()->role->permission['permission']['categories']['list']) && \Route::is('category')||
             empty(Auth::user()->role->permission['permission']['subCategories']['list']) && \Route::is('sub-category')||
             empty(Auth::user()->role->permission['permission']['subSubCategories']['list']) && \Route::is('sub-subcategory')||
             empty(Auth::user()->role->permission['permission']['coupon']['list']) && \Route::is('coupon')||
             empty(Auth::user()->role->permission['permission']['division']['list']) && \Route::is('add-division')||
             empty(Auth::user()->role->permission['permission']['district']['list']) && \Route::is('add-division')||
             empty(Auth::user()->role->permission['permission']['state']['list']) && \Route::is('add-state')||
             empty(Auth::user()->role->permission['permission']['report']['list']) && \Route::is('reports')||
             empty(Auth::user()->role->permission['permission']['review']['list']) && \Route::is('product-review-approve')||
             empty(Auth::user()->role->permission['permission']['stock']['list']) && \Route::is('product-stock')
            // empty(Auth::user()->role->permission['permission']['product']['list']) && \Route::is('manage-product')||
            // empty(Auth::user()->role->permission['permission']['product']['list']) && \Route::is('manage-product')||
            // empty(Auth::user()->role->permission['permission']['product']['list']) && \Route::is('manage-product')||
            // empty(Auth::user()->role->permission['permission']['product']['list']) && \Route::is('manage-product')||
        ){
            return response()->view('admin.home');
        }
    }
}